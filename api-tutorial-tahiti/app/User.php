<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Model
{
    use SoftDeletes;

    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password', 'id', 'image', 'image_resource', 'surname',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    //relaciones

    public function financing() {
        return $this->hasMany('App\Financing');
   }

}

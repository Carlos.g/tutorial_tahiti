<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\QueryException;
use App\Financing;

class FinancingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //buscar financiamientos en bd 
        $financings = Financing::all();
        //validar financinges
        if(!empty($financings)){
            $data = array(
                'code' => 200,
                'status' => 'success',
                'financings' => $financings,
            );
        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Financiaminetos no encontrados'
            );
        }

      return response()->json($data, $data['code']);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //recibe los datos
      $json = $request->input('json', null);
      $params_array = json_decode($json, true);
      $params = json_decode($json);

      if(!empty($params_array)){
          //limpiar datos
          $params_array=array_map('trim', $params_array);
          //validar datos
          $validator = Validator::make($params_array, [
              'name' => 'required',
              'description' => 'required',
              'link' => 'required',
          ]);
          if($validator->fails()){
              $data = array(
                  'code' => 400,
                  'status' => 'error',
                  'message' => 'Financiamiento no ha sido creado',
                  'errors' => $validator->errors()
              );
          }else{
              //crear objeto Financiamiento
              $financing = new Financing();
              //llenar objeto Financing
              $financing->name = $params_array['name'];
              $financing->description = $params_array['description'];
              $financing->link = $params_array['link'];
              $financing->user_id = $params_array['user_id'];
              //$financing->image = $params_array['image'];
              //$financing->image_resource = $params_array['image_resource'];
              
              //guardar en BD
              $financing->save();

              $data = array(
                  'code' => 200,
                  'status' => 'success',
                  'message' => 'financiamiento creado con exito',
                  'financing' => $financing
              );
          }
      }else{
          $data = array(
              'code' => 400,
              'status' => 'error',
              'message' => 'Datos incorrectos'
          );
      }
      return response()->json($data, $data['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //Sacar el financiamiento de la bd 
       $financig = Financing::find($id);
       //validar financiamiento
       if(!empty($financig)){
         $data = array(
            'code' => 200,
            'status' => 'success',
            'financing' => $financig
         );
       }else{
        $data = array(
           'code' => 400,
           'status' => 'error',
           'message' => 'financiamiento no encontrado'
        );
       }
     return response()->json($data, $data['code']);
    }

    //muestra los financiamientos del usuario del id

    public function showUser($id)
    {
       //Sacar el financiamiento de la bd 
       $financig = Financing::where('user_id', $id)->get();
       //validar financiamiento
       if(!empty($financig)){
         $data = array(
            'code' => 200,
            'status' => 'success',
            'financing' => $financig
         );
       }else{
        $data = array(
           'code' => 400,
           'status' => 'error',
           'message' => 'financiamiento no encontrado'
        );
       }
     return response()->json($data, $data['code']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
     //Recibir datos
     $json = $request->input('json', null);
     //$params = json_decode($json);
     $params_array = json_decode($json,true);
     //validar datos 
     if(!empty($params_array)){
         $validate = Validator::make($params_array, [
            'name' => 'required',
            'description' => 'required',
            'link' => 'required',
            'user_id' => 'required',
         ]);

         //sacar financing identificado
         $financing = Financing::find($params_array['id']);
         //validar Financing
         if(!$validate->fails()){    

             if(!empty($financing)){
                 //retirar los campos que no se actualizan
                 unset($params_array['created_at']);
                 unset($params_array['updated_at']);
               

                 //actualizar en la bd
                 $financing_update = Financing::where('id', $financing->id)->update($params_array);

                 //mensaje success
                 $data = array(
                     'code' => 200,
                     'status' => 'success',
                     'message' => 'El financing ha sido modificado',
                     'changes' => $params_array
                 );
             }else{
                 //mensaje de error
                 $data = array(
                     'code' => 400,
                     'status' => 'error',
                     'message' => 'financing no encontrada',
                 );
             }
         }else{
             $data = array(
                 'code' => 400,
                 'status' => 'error',
                 'message' => 'Datos invalidos',
                 'errors' => $validate->errors()
             );
         }   
     }else{
         $data = array(
             'code' => 400,
             'status' => 'error',
             'message' => 'Datos no recibidos'
         );
     }

     return response()->json($data, $data['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //buscar financing
      $financing = Financing::where('id',$id)->first();
      if(empty($financing)){
          $data = array(
              'code' => 400,
              'status' => 'error',
              'message' => 'financing no existe'
          );
      }else{

          //Eliminar financing
          $deleted = Financing::destroy($id);
          //validar bd
          if($deleted === 1){
              $data = array(
                 'code' => 200,
                 'status' => 'success',
                 'message' => 'Financing eliminada',
                 'financing' => $financing
              );
          }else{
             $data = array(
                 'code' => 400,
                 'status' => 'error',
                 'message' => 'Operacion fallida'
             );
          }
          
      }
      //devolver datos
      return response()->json($data, $data['code']);
    }

     //deshace el softdelete
     public function restore($id){
        //restaura la financing
        $op = Financing::onlyTrashed()->where('id', $id)->restore();
        //validar restauracion
        if($op === 1){
            $data = array(
                'code' => 200,
                'status' => 'success',
                'message' => 'financing restaurada'
            );
        }else{
            $data = array(
               'code' => 400,
               'status' => 'error',
               'message' => 'operacion no realizada'
            );
        }
        return response()->json($data, $data['code']);
    }

    //elimina por completo el modelo y sus modelos relacionados
    public function trueDelete($id){
        //buscar la Financing
        $financing = Financing::withTrashed()->find($id);
        //validar
        if(!empty($financing)){
            try{
             $deleted = $financing->forceDelete();
             if($deleted){
                $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'financing eliminada totalmente'
                );
             }else{
                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'financing no eliminada'
                );
             }
            }catch(QueryException $ex){
                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'error sql :'.$ex->getmessage()
                );
            }
            
        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'La financing no existe'
            );
        }
        return response()->json($data, $data['code']);
    }

    /*
     *Sube un archivo para el financing 
     *Sube los archivos a la carpeta Storage/app/public/financings/resources
    */ 

    public function upload (Request $request){
        //recibir datos
        $file = $request->file('file');
       
        //validar datos
        $validate = Validator::make($request->all(), [
            'file' => 'required|file|mimes: ' 
        ]);
        //guardar la imagen
        if(!$file && $validate->fails()){
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Error al subir archivo',
                'errors' => $validate->errors()
            );
        }else{
            
            $file_name = time().$file->getClientOriginalName();
            $path = \Storage::disk('public')->putFileAs(
                'financings/resources', $file, $file_name
            );
            //modificar url de la imagen
            $url = 'storage/'.$path;
            $data = array(
                'code' => 200,
                'status' => 'access',
                'image' => $file_name,
                'image_resource' => $url
            );
        }
        return response()->json($data, $data['code']);
   }
}

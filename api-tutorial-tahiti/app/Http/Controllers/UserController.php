<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\User;


class UserController extends Controller{

    public function index(Request $request){
        //traer todos los usuarios
            $users = User::all();
        //validar los usuarios
            if(!empty($users)){
                $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'users' => $users
                );
            }else{
                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'usuarios no encontrados'
                );
            }
        //devolver respuesta
        return response()->json($data, $data['code']);
    }


    public function register(Request $request){
        //recibir datos
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);
        $params = json_decode($json);

        //limpiar datos
        if(!empty($params_array)){
            $params_array=array_map('trim', $params_array);

            //validar datos
            $validator = Validator::make($params_array,[
                    'name' => 'required|alpha',
                    'surname' => 'required|alpha',
                    'email' => 'required|unique:users|email',
                    'password' => 'required'
            ]);

            if($validator->fails()){
                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'El usuario no ha sido creado',
                    'errors' => $validator->errors()
                );
            }else{
                //cifrar la contraseña
                $pwd = hash('sha256', $params->password);

                //crear el usuario y adicionar parametros
                $user = new User();
                $user->name = $params_array['name'];
                $user->surname = $params_array['surname'];
                $user->email = $params_array['email'];
                $user->password = $pwd;
                if(isset($params_array['role'])){
                    $user->role =$params_array['role'];
                }
                

                //guardar usuario en la BD

                $user->save();

                $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'El usuario ha sido creado con exito',
                    'user' => $user
                );
            }
        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Datos incorrectos'
            );
        }

        return response()->json($data, $data['code']);
       
    }

    public function login(Request $request){
        $jwtAuth = new \JwtAuth();

        //Recibir los datos
        $json = $request->input('json', null);
        $params = json_decode($json);
        $params_array = json_decode($json, true);

        //validar datos

        //var_dump($params_array); die();

        $validate = Validator::make($params_array, [
                'email' => 'required|email',
                'password' => 'required'
        ]);

        if($validate->fails()){
            //fallo la validacion
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Datos erroneos',
                'errors' => $validate->errors()
            );  
        }else{
            //cifrar la contraseña
            $pwd = hash('sha256',$params->password);

            //devolver token o la data
            $data = $jwtAuth->singup($params->email, $pwd, $params->getidentity);
        }

        return response()->json($data, $data['code']);

    }

   
    public function details($id)
    {
        //Sacar usuario identificado
        $user = User::with('financing')->find($id);
        //validar usuario existe
        if(!empty($user) && is_object($user)){
            $data = array(
                'code' => 200,
                'status' => 'success',
                'user' => $user
            );
        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Usuario no encontrado'
            );
        }

       return response()->json($data, $data['code']); 
    }


    public function update(Request $request){
        //comprobar la autorizacion
        $token = $request->header('Authorization');
        $jwtAuth = new \JwtAuth();
        $checkToken = $jwtAuth->checkToken($token);
    
        //Recibir los datos
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);

        if($checkToken && !empty($params_array)){
            //Validar datos
            $validate = Validator::make($params_array, [
                'name' => 'required',
                'surname' => 'required',
                'email' => 'required|email'
            ]);
           if(!$validate->fails()){

               //sacar usuario
               $user = $jwtAuth->checkToken($token, true);
               $user = User::find($user->sub);
               //var_dump($user->id); die();
               //Eliminar recursos
               if(isset($params_array['deleteResource'])){
                 if(!is_null($user->image)){
                   if($params_array['deleteResource']){
                    $deleter = new \ResourceDeleter();
                    $deleted = $deleter->deleteUserResources($user->id);
                    unset($params_array['deleteResource']);
                   }   
                 }   
               }

               if(isset($params_array['deleteResource'])) unset($params_array['deleteResource']);

               $params_array['image_resource'] = $params_array['imageResource'];
   
               //quitar los campos que no quiero actulizar
               unset($params_array['id']);
               unset($params_array['role']);
               unset($params_array['password']);
               unset($params_array['created_at']);
               unset($params_array['remember_token']);
               unset($params_array['imageResource']);
               unset($params_array['lastUnit']);
               
               //actualizar BD
               $user_update = User::where('id', $user->id)->update($params_array);
               //devolver array con el resultado    
               if(isset($deleted)){
                   $data = array(
                       'code' => 200,
                       'status' => 'success',
                       'message' => 'El usuario ha sido modificado',
                       'resource_deleted' => $deleted,
                       'changes' => $params_array
                   );
               }else{
                   $data = array(
                       'code' => 200,
                       'status' => 'success',
                       'message' => 'El usuario ha sido modificado',
                       'changes' => $params_array
                   );
               }
           }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'datos incorrectos '. $validate->errors(),
            );
           } 

        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Usuario no autenticado',
            );
        }
        return response()->json($data, $data['code']);
    }


    public function updateGrade(Request $request){
        //comprobar la autorizacion
        $token = $request->header('Authorization');
        $jwtAuth = new \JwtAuth();
        $checkToken = $jwtAuth->checkToken($token);
    
        //Recibir los datos
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);

        if($checkToken && !empty($params_array)){
            //Validar datos
            $validate = Validator::make($params_array, [
                'lastUnit' => 'required',
            ]);
           if(!$validate->fails()){

               //sacar usuario
               $user = $jwtAuth->checkToken($token, true);
               $user = User::find($user->sub);
               
               if(isset($params_array['deleteResource'])) unset($params_array['deleteResource']);
               
               //actualizar BD
               $user_update = User::where('id', $user->id)->update($params_array);
               //devolver array con el resultado    
               $data = array(
                   'code' => 200,
                   'status' => 'success',
                   'message' => 'ultima unidad ha sido modificado',
                   'changes' => $params_array
               );
           }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'datos incorrectos '. $validate->errors(),
            );
           } 

        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Usuario no autenticado',
            );
        }
        return response()->json($data, $data['code']);
    }

    
    public function upload(Request $request){
        //recibir datos
        $file = $request->file('file');
        
        //validar datos
        $validate = Validator::make($request->all(), [
            'file' => 'required|image|mimes: jpg,jpeg,png,gif' 
        ]);
        //guardar la imagen
        if(!$file && $validate->fails()){
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Error al subir imagen',
                'errors' => $validate->errors()
            );
        }else{
            $file_name = time().$file->getClientOriginalName();
            $path = \Storage::disk('public')->putFileAs(
                'avatars', $file, $file_name
            );
            //modificar url de la imagen
            $url = 'storage/'.$path;
            $data = array(
                'code' => 200,
                'status' => 'access',
                'image' => $file_name,
                'image_resource' => $url
            );
        }
        return response()->json($data, $data['code']);
    }

    public function destroy($id){
        //buscar usuario
        $user = User::where('id',$id)->first();
        if(empty($user)){
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Usuario no existe'
            );
        }else{
            if($user->role === 'ROLE_ADMIN'){
                $data = array(
                    'code' => 403,
                    'status' => 'error',
                    'message' => 'Permisos invalidos'
                );
            }else{
                //Eliminar usuario
                $deleted = User::destroy($id);
                //validar bd
                if($deleted === 1){
                    $data = array(
                        'code' => 200,
                        'status' => 'success',
                        'message' => 'Usuario eliminado',
                        'User' => $user
                    );
                }else{
                    $data = array(
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'Operacion fallida'
                    );
                }
            } 
        }
        //devolver datos
        return response()->json($data, $data['code']);
    }
    //funcion de prueba para re crear los datos borrados
    public function restore($id){
       //restaura la unidad
       $op = User::onlyTrashed()->where('id', $id)->restore();
       //validar restauracion
       if($op === 1){
           $data = array(
               'code' => 200,
               'status' => 'success',
               'message' => 'Usuario restaurado'
           );
       }else{
           $data = array(
              'code' => 400,
              'status' => 'error',
              'message' => 'operacion no realizada'
           );
       }
       return response()->json($data, $data['code']);
    }
}

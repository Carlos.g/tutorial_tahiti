<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\QueryException;
use App\Chapter; 

class ChapterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //buscar capitulos en bd con sus capitulos
        $chapters = Chapter::with('tutorial')->get();
        //validar capitulos
        if(!empty($chapters)){
            $data = array(
                'code' => 200,
                'status' => 'success',
                'chapters' => $chapters
            );
        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Capitulos no encontrados'
            );
        }

      return response()->json($data, $data['code']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //recibe los datos
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);
        $params = json_decode('json');

        if(!empty($params_array)){
            //limpiar datos
            $params_array=array_map('trim', $params_array);
            //validar datos
            $validator = Validator::make($params_array, [
                'name'  => 'required',
                'description' => 'required',
                'image' => 'required',
                'image_resource' => 'required',
                'unit_id' => 'required|numeric'      
            ]);
            if($validator->fails()){
                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Capitulo no ha sido creado',
                    'errors' => $validator->errors()
                );
            }else{
                //crear objeto unidad
                $chapter = new Chapter();
                //llenar objeto unidad
                $chapter->name = $params_array['name'];
                $chapter->description = $params_array['description'];
                $chapter->image = $params_array['image'];
                $chapter->image_resource = $params_array['image_resource'];
                $chapter->unit_id = $params_array['unit_id'];
                if(isset( $params_array['text'])){
                    $chapter->text = $params_array['text'];
                }

                //guardar en BD
                $chapter->save();

                $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'Capitulo creado con exito',
                    'chapter' => $chapter
                );
            }
        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Datos incorrectos'
            );
        }
        return response()->json($data, $data['code']);
    } 
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Sacar el capitulo de la bd con todos sus tutoriales
        $chapter = Chapter::with('tutorial')->find($id);
        //validar capitulo
        if(!empty($chapter) && is_object($chapter)){
            $data = array(
                'code' => 200,
                'status' => 'success',
                'chapter' => $chapter
            );
        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Capitulo no encontrada'
            );
        }
       return response()->json($data, $data['code']); 
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Recibir datos
        $json = $request->input('json', null);
        //$params = json_decode($json);
        $params_array = json_decode($json,true);
        //validar datos 
        if(!empty($params_array)){
            $validate = Validator::make($params_array, [
                'name' => 'required',
                'description' => 'required',
                'image' => 'required',
                //'text' => 'required',
                'image' => 'required',
                'image_resource' => 'required',
            ]);

            //sacar capitulo identificado
            $chapter = Chapter::find($id);
            //validar unidad
            if(!$validate->fails()){    
                if(!empty($chapter)){
                    //retirar los campos que no se actualizan
                    unset($params_array['id']);
                    unset($params_array['created_at']);
                    unset($params_array['updated_at']);
                    //unset($params_array['deleted_at']);

                    //actualizar en la bd
                    $chapter_update = $chapter->update($params_array);

                    //mensaje success
                    $data = array(
                        'code' => 200,
                        'status' => 'success',
                        'message' => 'El capitulo ha sido modificado',
                        'changes' => $params_array
                    );
                }else{
                    //mensaje de error
                    $data = array(
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'Capitulo no encontrado',
                    );
                }
            }else{
                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Datos invalidos',
                    'errors' => $validate->errors()
                );
            }   
        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Datos no recibidos'
            );
        }
        return response()->json($data, $data['code']);
    }

    /**
     * Remove the specified resource with softdeletes
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         //buscar unidad
         $chapter = Chapter::where('id',$id)->first();
         if(empty($chapter)){
             $data = array(
                 'code' => 400,
                 'status' => 'error', 
                 'message' => 'Unidad no existe'
             );
         }else{

             //Eliminar usuario
             $deleted = chapter::destroy($id);
             //validar bd
             if($deleted === 1){
                 $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'Unidad eliminada',
                    'chapter' => $chapter
                 );
             }else{
                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Operacion fallida'
                );
             }
             
         }
         //devolver datos
         return response()->json($data, $data['code']);
    }


    //deshace el softdelete
    public function restore($id){
        //restaura la unidad
        $op = Chapter::onlyTrashed()->where('id', $id)->restore();
        //validar restauracion
        if($op === 1){
            $data = array(
                'code' => 200,
                'status' => 'success',
                'message' => 'Capitulo restaurado'
            );
        }else{
            $data = array(
               'code' => 400,
               'status' => 'error',
               'message' => 'operacion no realizada'
            );
        }
        return response()->json($data, $data['code']);
    }

    /*
     elimina por completo el modelo
     no se puede eliminar si existen modelos que hagan referencia a ese modelo
     ej: un tutorial hace referencia a un capitulo, este capitulo no se puede borrar
    */  
    public function trueDelete($id){
        //buscar la unidad
        $chapter = Chapter::withTrashed()->find($id);
        //validar
        if(!empty($chapter)){
            try{
             $deleted = $chapter->forceDelete();
             if($deleted){
                $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'Capitulo eliminado totalmente'
                );
             }else{
                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Chapitulo no eliminado'
                );
             }
            }catch(QueryException $ex){
                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'error sql :'.$ex->getmessage()
                );
            }
        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'El capitulo no existe'
            );
        }
        return response()->json($data, $data['code']);
    }

    /*
     *Sube un archivo imagen para el capitulo 
     *Sube los archivos a la carpeta Storage/app/public/chapters/images
    */ 
    public function upload (Request $request){
        //recibir datos
        $file = $request->file('file');
       
        //validar datos
        $validate = Validator::make($request->all(), [
            'file' => 'required|image|mimes: jpg,jpeg,png,gif' 
        ]);
        //guardar la imagen
        if(!$file && $validate->fails()){
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Error al subir imagen',
                'errors' => $validate->errors()
            );
        }else{
            
            $file_name = time().$file->getClientOriginalName();
            $path = \Storage::disk('public')->putFileAs(
                'chapters/images', $file, $file_name
            );
            //modificar url de la imagen
            $url = 'storage/'.$path;
            $data = array(
                'code' => 200,
                'status' => 'access',
                'image' => $file_name,
                'image_resource' => $url
            );
        }
        return response()->json($data, $data['code']);
   }

   public function uploadPDF (Request $request){
    //recibir datos
    $file = $request->file('file');
   
    //validar datos
    $validate = Validator::make($request->all(), [
        'file' => 'required|mimes: pdf' 
    ]);
    //guardar la imagen
    if(!$file && $validate->fails()){
        $data = array(
            'code' => 400,
            'status' => 'error',
            'message' => 'Error al subir documento',
            'errors' => $validate->errors()
        );
    }else{
        
        $file_name = time().$file->getClientOriginalName();
        $path = \Storage::disk('public')->putFileAs(
            'chapters/pdf', $file, $file_name
        );
        //modificar url de la imagen
        $url = 'storage/'.$path;
        $data = array(
            'code' => 200,
            'status' => 'access',
            'document' => $file_name,
            'documet_resource' => $url
        );
    }
    return response()->json($data, $data['code']);
   }
    
}

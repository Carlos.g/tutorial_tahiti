<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\QueryException;
use App\Unit; 

class UnitController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        //buscar unidades en bd con sus capitulos
        $units = Unit::with('chapter')->get();
        //validar unidades
        if(!empty($units)){
            $data = array(
                'code' => 200,
                'status' => 'success',
                'units' => $units
            );
        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Unidades no encontradas'
            );
        }

      return response()->json($data, $data['code']);  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //recibe los datos
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);
        $params = json_decode('json');

        if(!empty($params_array)){
            //limpiar datos
            $params_array=array_map('trim', $params_array);
            //validar datos
            $validator = Validator::make($params_array, [
                'name' => 'required',
                'description' => 'required',
                'image' => 'required',
                'image_resource' => 'required'           
            ]);
            if($validator->fails()){
                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Unidad no ha sido creada',
                    'errors' => $validator->errors()
                );
            }else{
                //crear objeto unidad
                $unit = new Unit();
                //llenar objeto unidad
                $unit->name = $params_array['name'];
                $unit->description = $params_array['description'];
                $unit->image = $params_array['image'];

                //guardar en BD
                $unit->save();

                $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'Unidad creada con exito',
                    'unit' => $unit
                );
            }
        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Datos incorrectos'
            );
        }
        return response()->json($data, $data['code']);
    } 
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Sacar la unidad de la bd con todos sus capitulos
        $unit = Unit::with('chapter')->find($id);
        //validar unidad
        if(!empty($unit) && is_object($unit)){
            $data = array(
                'code' => 200,
                'status' => 'success',
                'unit' => $unit
            );
        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Unidad no encontrada'
            );
        }
       return response()->json($data, $data['code']); 
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        //Recibir datos
        $json = $request->input('json', null);
        //$params = json_decode($json);
        $params_array = json_decode($json,true);
        //validar datos 
        if(!empty($params_array)){
            $validate = Validator::make($params_array, [
                'name' => 'required',
                'description' => 'required',
                'image' => 'required',
                'image_resource' => 'required'
            ]);

            //sacar unidad identificada
            $unit = Unit::find($id);
            //validar unidad
            if(!$validate->fails()){    
                if(!empty($unit)){
                    //retirar los campos que no se actualizan
                    unset($params_array['id']);
                    unset($params_array['created_at']);
                    unset($params_array['updated_at']);
                    //unset($params_array['deleted_at']);

                    //actualizar en la bd
                    $unit_update = $unit->update($params_array);

                    //mensaje success
                    $data = array(
                        'code' => 200,
                        'status' => 'success',
                        'message' => 'La unidad ha sido modificada',
                        'changes' => $params_array
                    );
                }else{
                    //mensaje de error
                    $data = array(
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'Unidad no encontrada',
                    );
                }
            }else{
                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Datos invalidos',
                    'errors' => $validate->errors()
                );
            }   
        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Datos no recibidos'
            );
        }

        return response()->json($data, $data['code']);
    }

    /**
     * Remove the specified resource from storage with softDelete.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
         //buscar unidad
         $unit = Unit::where('id',$id)->first();
         if(empty($unit)){
             $data = array(
                 'code' => 400,
                 'status' => 'error',
                 'message' => 'Unidad no existe'
             );
         }else{

             //Eliminar usuario
             $deleted = Unit::destroy($id);
             //validar bd
             if($deleted === 1){
                 $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'Unidad eliminada',
                    'Unit' => $unit
                 );
             }else{
                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Operacion fallida'
                );
             }
             
         }
         //devolver datos
         return response()->json($data, $data['code']);
    }

    //deshace el softdelete
    public function restore($id){
        //restaura la unidad
        $op = Unit::onlyTrashed()->where('id', $id)->restore();
        //validar restauracion
        if($op === 1){
            $data = array(
                'code' => 200,
                'status' => 'success',
                'message' => 'Unidad restaurada'
            );
        }else{
            $data = array(
               'code' => 400,
               'status' => 'error',
               'message' => 'operacion no realizada'
            );
        }
        return response()->json($data, $data['code']);
    }

    //elimina por completo el modelo y sus modelos relacionados
    public function trueDelete($id){
        //buscar la unidad
        $unit = Unit::withTrashed()->find($id);
        //validar
        if(!empty($unit)){
            try{
             $deleted = $unit->forceDelete();
             if($deleted){
                $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'Unidad eliminada totalmente'
                );
             }else{
                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Unidad no eliminada'
                );
             }
            }catch(QueryException $ex){
                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'error sql :'.$ex->getmessage()
                );
            }
            
        }else{
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'La unidad no existe'
            );
        }
        return response()->json($data, $data['code']);
    }


    /*
     *Sube un archivo imagen para la unidad 
     *Sube los archivos a la carpeta Storage/app/public/units/images
    */ 

    public function upload (Request $request){
         //recibir datos
         $file = $request->file('file');
        
         //validar datos
         $validate = Validator::make($request->all(), [
             'file' => 'required|image|mimes: jpg,jpeg,png,gif' 
         ]);
         //guardar la imagen
         if(!$file && $validate->fails()){
             $data = array(
                 'code' => 400,
                 'status' => 'error',
                 'message' => 'Error al subir imagen',
                 'errors' => $validate->errors()
             );
         }else{
             
             $file_name = time().$file->getClientOriginalName();
             $path = \Storage::disk('public')->putFileAs(
                 'units/images', $file, $file_name
             );
             //modificar url de la imagen
             $url = 'storage/'.$path;
             $data = array(
                 'code' => 200,
                 'status' => 'access',
                 'image' => $file_name,
                 'image_resource' => $url
             );
         }
         return response()->json($data, $data['code']);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class ApiAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //comprobar la autenticacion
        $token = $request->header('Authorization');
        $jwtAuth = new \JwtAuth();
        $checkToken = $jwtAuth->checkToken($token);
        
        if($checkToken){
        return $next($request);
        }
        $data = array(
            'code' => 400,
            'status' => 'failed',
            'message' => 'error al autenticar'
        );
        return response()->json($data, $data['code']);
    }
}

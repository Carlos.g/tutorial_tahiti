<?php

namespace App\Http\Middleware;

use Closure;

class AuthInitMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //comprobar la autenticacion
        $token = $request->header('Authorization');
        $jwtAuth = new \JwtAuth();
        $checkToken = $jwtAuth->checkToken($token);
        //var_dump($checkToken); die();
        if($checkToken){
            $data = array(
                'code' => 200,
                'status' => 'succes',
                'message' => 'token correcto',
            );
        }else{
            $data = array(
                'code' => 400,
                'status' => 'failed',
                'message' => 'token vencido',
            );
        }
        return response()->json($data, $data['code']);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class AdminAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //recibir el token
        $token = $request->header('Authorization');
        //revizar tipo usuario
        $jwtAuth = new \JwtAuth();
        $checkRole = $jwtAuth->checkRole($token);
        if($checkRole){
            //redireccion al controlador de usuarios
            return $next($request);
        }
        //respuesta de error
        $data = array(
            'code' => 400,
            'status' => 'failed',
            'message' => 'error al autenticar'
        );
        return response()->json($data, $data['code']);
    }
}

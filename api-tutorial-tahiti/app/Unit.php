<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    use SoftDeletes;

    protected $table = 'units';

    protected $fillable = [
        'id', 'name', 'description', 'image', 'image_resource'
    ];

    protected $hidden = [
        'deleted_at'
    ];
        
    

    //relaciones

    public function chapter()
    {
        return $this->hasMany('App\Chapter');
    }

}

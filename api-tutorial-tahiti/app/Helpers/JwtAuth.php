<?php

namespace App\Helpers;

use Firebase\JWT\JWT;
use App\User;
use App\Exceptions\Handler;

class JwtAuth {

    private $key;

    public function __construct(){
        $this->key = 'esta es tu clave secreta carechimba-00123456781';
    }

    public function singUp($email, $password, $getIdentity = false){
        //buscar al usuario con las credenciales
        $user = User::where([
            'email' => $email,
            'password' => $password
        ])->first();
        
        //comprobar si el usuario existe 
        $singup = false;
        
        if(is_object($user)){
            $singup=true;
        }
        //generar el token exp es la fecha de expiracion del token
        if($singup){
            $token = array(
                'sub' => $user->id,
                'email' => $user->email,
                'name' => $user->name,
                'surname' => $user->surname,
                'lastUnit' => $user->lastUnit,
                'image' => $user->image,
                'image_resource' => $user->image_resource,
                'description' => $user->description,
                'role' => $user->role,
                'iat' => time(),
                'exp' => time() + (7 * 24 * 60 * 60)
            );

            $jwt = JWT::encode($token, $this->key, 'HS256');
            $decoded = JWT::decode($jwt,$this->key, ['HS256']);

            //devolver los datos decodificados y el token, en funcion del parametro getToken
            if(!$getIdentity){
                $data = [
                   'code'  => 200,
                   'status'=> 'success',
                   'token' => $jwt,
                ];
            }else{
                $data =  [
                    'code'  => 200,
                    'user'  => $decoded,
                    'token' => $jwt,
                ];
            }
        }else{
            $data = array(
                'code'    => 400,
                'status'  => 'error',
                'message' => 'Usuario incorrecto'
            );
        }
        return $data;
    }

    public function checkToken($jwt, $getIdentity = false){
        $auth = false;
        if(!empty($jwt)){
            try {
                $jwt = str_replace('"', "", $jwt);
                $decode = JWT::decode($jwt, $this->key, ['HS256']);
            } catch (\UnexpectedValueExeption $ex) {
                $auth = false;
            } catch (\DomainException $ex) {
                $auth = false;
            } catch (\Firebase\JWT\ExpiredException $ex) {
                $auth = false;
            }

            if(!empty($decode) && is_object($decode) && isset($decode->sub)){
                $auth = true;
            }else{
                $auth = false;
            }
        }else{
            $auth = false;
        }
        if($getIdentity && isset($decode)){
            return $decode;
        }
        return $auth;
    }

    public function checkRole($jwt){
        $auth = false;
        //decodificar el token
        $info = $this->checkToken($jwt, true);
        //validar role
        if(is_object($info)){
            if($info->role === 'ROLE_ADMIN'){
                $auth = true;
            }else{
                $auth = false;
            }
        }   
        return $auth;
    }



}
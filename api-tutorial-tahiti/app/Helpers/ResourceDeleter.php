<?php


 namespace App\Helpers;

 use App\User;
 use App\Unit;
 use App\Exam;
 use App\Tutorial;
 use App\Chapter;
 use App\Grade;
 use App\Exceptions\Handler;
/*
 Esta clase posee metodos para eliminar los recursos no usados que se modifiquen en el api
 por ejempo si se modifica la imagen de una unidad la imagen anterior debe ser eliminada para no malgastar espacio
 en el servidor, para esto sirve esta clase
*/
 class ResourceDeleter {

    public function deleteUserResources($id){
        $deleted = false;
        //buscar el usuario
        $user = User::find($id);
        //verificar imagen
        if(!empty($user->image)){
            $operation = \Storage::disk('public')->delete('avatars/'.$user->image);
            if($operation){
                $user->update(['image' => null, 'image_resource' => null]);
                $deleted = true;
            }else{
                $deleted = false;
            } 
        }else{
            $deleted = false;
        }
       return $deleted; 
    } 

 }
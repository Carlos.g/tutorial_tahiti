<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Financing extends Model
{
    use softDeletes;

    protected $table = 'financings';

    protected $fillable = [
        'name', 'description', 'link', 'image', 'image_resource', 'user_id',
    ];

    public function user (){
        return $this->belongsTo('App\User');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chapter extends Model
{
    use SoftDeletes;

    protected $table = 'chapters';

    protected $fillable = [
        'name', 'description', 'text', 'image', 'image_resource', 'video', 'paper'
    ];

    protected $hidden = [
        'deleted_at'
    ];

    //relaciones

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

}

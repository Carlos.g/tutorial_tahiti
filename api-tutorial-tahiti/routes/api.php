<?php

use Illuminate\Http\Request;
use App\Http\Middleware\ApiAuthMiddleware;
use App\Http\Middleware\AdminAuthMiddleware;
use App\Http\Middleware\AuthInitMiddleware;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//RUTAS API: USER
Route::get('user', 'UserController@index')->middleware('admin.auth');
Route::get('user/{id}', 'UserController@details');
Route::get('init', function () {})->middleware('init.auth');
Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::post('user/upload', 'UserController@upload')->middleware('api.auth');
Route::post('user/update', 'UserController@update')->middleware('api.auth');
Route::post('user/update/grade', 'UserController@updateGrade')->middleware('api.auth');
Route::delete('user/{id}', 'UserController@destroy')->middleware('admin.auth');
Route::get('user/restore/{id}', 'UserController@restore')->middleware('admin.auth');


//RUTAS API: UNIT
Route::resource('unit', 'UnitController')->except([
    'index', 'show', 'create', 'edit'
])->middleware('admin.auth');
Route::get('unit', 'UnitController@index')->middleware('api.auth');
Route::get('unit/{id}', 'UnitController@show')->middleware('api.auth');
Route::post('unit/upload', 'UnitController@upload')->middleware('admin.auth');
Route::get('unit/restore/{id}', 'UnitController@restore')->middleware('admin.auth');
Route::delete('unit/destroy/{id}', 'UnitController@trueDelete')->middleware('admin.auth');


//RUTAS API: CHAPTER
Route::resource('chapter', 'ChapterController')->except([
    'index', 'show', 'create', 'edit'
])->middleware('admin.auth');
Route::get('chapter', 'ChapterController@index')->middleware('api.auth');
Route::get('chapter/{id}', 'ChapterController@show')->middleware('api.auth');
Route::post('chapter/upload', 'ChapterController@upload')->middleware('admin.auth');
Route::post('chapter/upload/pdf', 'ChapterController@uploadPDF')->middleware('admin.auth');
Route::get('chapter/restore/{id}', 'ChapterController@restore')->middleware('admin.auth');
Route::delete('chapter/destroy/{id}', 'ChapterController@trueDelete')->middleware('admin.auth');


/*//RUTAS API: TUTORIAL
Route::resource('tutorial', 'TutorialController')->except([
    'index', 'show', 'create', 'edit'
])->middleware('admin.auth');
Route::get('tutorial', 'TutorialController@index')->middleware('api.auth');
Route::get('tutorial/{id}', 'TutorialController@show')->middleware('api.auth');
Route::post('tutorial/upload', 'TutorialController@upload')->middleware('admin.auth');
Route::get('tutorial/restore/{id}', 'TutorialController@restore')->middleware('admin.auth');
Route::post('tutorial/destroy/{id}', 'TutorialController@trueDelete')->middleware('admin.auth');
*/

//RUTAS API: FINANCING
Route::get('financing', 'FinancingController@index')->middleware('api.auth');
Route::get('financing/{id}', 'FinancingController@show')->middleware('api.auth');
Route::get('financing/user/{id}', 'FinancingController@showUser')->middleware('api.auth');
Route::post('financing/upload', 'FinancingController@upload')->middleware('admin.auth');
Route::post('financing', 'FinancingController@store')->middleware('api.auth');
Route::post('financing/update', 'FinancingController@update')->middleware('api.auth');
Route::post('financing/delete', 'FinancingController@destroy')->middleware('admin.auth');
Route::get('financing/restore/{id}', 'FinancingController@restore')->middleware('admin.auth');
Route::post('financing/destroy/{id}', 'FinancingController@trueDelete')->middleware('api.auth');


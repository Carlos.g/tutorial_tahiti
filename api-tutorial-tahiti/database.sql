CREATE DATABASE IF NOT EXISTS tutorial_tahiti;

USE tutorial_tahiti;

CREATE TABLE users(
id                  int(255) auto_increment not null,
name                varchar(50) not null,               
role                varchar(20),
email               varchar(255) not null,
surname             varchar(100),
password            varchar(255) not null,
description         varchar(255),
created_at          datetime DEFAULT NULL,
updated_at         datetime DEFAULT NULL,
image               varchar(255),
CONSTRAINT pk_users PRIMARY KEY(id)
)ENGINE=InnoDb;
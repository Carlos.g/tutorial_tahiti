//Necesarias
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:tahiti_app/src/providers/users_provider.dart';
//Propias
import 'package:tahiti_app/src/routes/routes.dart';
import 'package:tahiti_app/src/pages/home_page.dart';
import 'package:tahiti_app/src/shared_prefs/preferencias_usuario.dart';

void main() async {

  //llamada de las preferencias de usuario
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = PreferenciasUsuario();
  final userProvider = UserProvider();
  //llamada para confirmar token
  await prefs.initPrefs();
  await userProvider.revToken(); 
  
  runApp(TahitiApp());
} 
 
class TahitiApp extends StatelessWidget {

  final prefs = PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          iconTheme: IconThemeData(color: Colors.black),
          color: Colors.green,
        ),
        canvasColor: Colors.white,
      ),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('es', 'ES'),
      ],
      debugShowCheckedModeBanner: false,
      title: 'Tahiti App',
      initialRoute: (prefs.token != '') ? 'HomePage' : 'login',
      routes: getApplicationRoutes(),
      onGenerateRoute: (RouteSettings settings){
        return MaterialPageRoute(
          builder: (BuildContext context) => HomePage()
        );
      },
    );
  }
}
import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {

  static final PreferenciasUsuario _instancia = PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  } 

  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  // Get y set Token

  get token {
    return _prefs.getString('token') ?? '';
  }

  set token(String value){
    _prefs.setString('token', value);
  }

  
  // Get y set nombre

  get name {
    return _prefs.getString('nombre') ?? null;
  }

  set name(String value){
    _prefs.setString('nombre', value);
  }

   
  // Get y set surname

  get surname {
    return _prefs.getString('surname') ?? null;
  }

  set surname(String value){
    _prefs.setString('surname', value);
  }

  
  // Get y set image resource

  get imageResource {
    return _prefs.getString('image_resource') ?? null;
  }

  set imageResource(String value){
    _prefs.setString('image_resource', value);
  }

  // Get y set description

  get description {
    return _prefs.getString('description') ?? null;
  }

  set description(String value){
    _prefs.setString('description', value);
  }

  // Get y set id

  get id {
    return _prefs.getInt('id') ?? null;
  }

  set id(int value){
    _prefs.setInt('id', value);
  }

  get lastUnit {
    return _prefs.getInt('lastUnit') ?? null;
  }

  set lastUnit(int value){
    _prefs.setInt('lastUnit', value);
  }

  get email {
    return _prefs.getString('email') ?? null;
  }

  set email(String value){
    _prefs.setString('email', value);
  }

  get role {
    return _prefs.getString('role') ?? null;
  }

  set role(String value){
    _prefs.setString('role', value);
  }

  get image {
    return _prefs.getString('image') ?? null;
  }

  set image(String value){
    _prefs.setString('image', value);
  }


  getImage(){
    if(imageResource == null) return 'https://imgclasificados5.emol.com/Publicador/116/F11922917111412972471611595531933226122116.png';
    
    return 'http://tutorial-tahiti.000webhostapp.com/$imageResource';
  }

  logout() {
     name          = null;
     surname       = null;
     token         = null;
     description   = null;
     imageResource = null;
     image         = null;
     id            = null;
     lastUnit      = null;
     role          = null;
   }
}
import 'package:flutter/material.dart';

//paginas
import 'package:tahiti_app/src/pages/chapter_page.dart';
import 'package:tahiti_app/src/pages/financiamiento_menu_page.dart';
import 'package:tahiti_app/src/pages/financing_edit_page.dart';
import 'package:tahiti_app/src/pages/login_page.dart';
import 'package:tahiti_app/src/pages/home_page.dart';
import 'package:tahiti_app/src/pages/maual_page.dart';
import 'package:tahiti_app/src/pages/pdfViewer_page.dart';
import 'package:tahiti_app/src/pages/register_page.dart';
import 'package:tahiti_app/src/pages/tutorial_page.dart';
import 'package:tahiti_app/src/pages/financiamiento_page.dart';
import 'package:tahiti_app/src/pages/ayuda_page.dart';
import 'package:tahiti_app/src/pages/user_settings_page.dart';



Map<String, WidgetBuilder> getApplicationRoutes(){
    return <String, WidgetBuilder>{
      'login'            : (BuildContext context) => LoginPage(),
      'home'             : (BuildContext context) => HomePage(),
      'tutorial'         : (BuildContext context) => TutorialPage(),
      'financing'        : (BuildContext context) => FinanciamientoPage(),
      'ayuda'            : (BuildContext context) => AyudaPage(),
      'settings'         : (BuildContext context) => UserSettingsPage(),
      'chapters'         : (BuildContext context) => ChaptersPage(),
      'pdf'              : (BuildContext context) => PdfViewerPage(),
      //'financig_details' : (BuildContext context) => FinancingDetailsPage(),
      //'unity'            : (BuildContext context) => UnityDemoScreen(),
      'register'         : (BuildContext context) => RegisterPage(),
      'financingEdit'    : (BuildContext context) => FinancingEditPage(),
      'financingMenu'    : (BuildContext context) => FinancingMenu(),
      'Manual'           : (BuildContext context) => ManualPage(),
      //'financingCreate'  : (BuildContext context) => FinancingCreate(),
    };
}   
import 'package:flutter/material.dart';

import 'package:tahiti_app/src/shared_prefs/preferencias_usuario.dart';


class MenuDrawer extends StatelessWidget {

  final prefs = PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
           decoration: BoxDecoration(
               image: DecorationImage(
                 image: AssetImage('assets/img/menu-img.jpg'),
                 fit: BoxFit.fill,
               ),
           ), 
           child: Container(),
          ),

          ListTile(
            leading: Icon(Icons.pages, color: Colors.blue),
            title: Text('Home', style: TextStyle(color: Colors.black),),
            onTap: () {
              Navigator.popAndPushNamed(context, 'home');
            } 
          ),

          ListTile(
            leading: Icon(Icons.art_track, color: Colors.blue),
            title: Text('Tutorial', style: TextStyle(color: Colors.black)),
            onTap: () {
              Navigator.popAndPushNamed(context, 'tutorial');
            }
          ),

          ListTile(
            leading: Icon(Icons.account_balance_wallet, color: Colors.blue),
            title: Text('Metodos de financiacion', style: TextStyle(color: Colors.black)),
            onTap: () {
              Navigator.popAndPushNamed(context, 'financing');
            }
          ),

          ListTile(
            leading: Icon(Icons.settings, color: Colors.blue),
            title: Text('Usuario', style: TextStyle(color: Colors.black)),
            onTap: () {
              Navigator.popAndPushNamed(context, 'settings');
            }
          ),

          ListTile(
            leading: Icon(Icons.help, color: Colors.blue),
            title: Text('Ayuda', style: TextStyle(color: Colors.black)),
            onTap: () {
              Navigator.popAndPushNamed(context, 'ayuda');
            }
          ),

          ListTile(
            leading: Icon(Icons.subdirectory_arrow_left, color: Colors.blue),
            title: Text('Logout', style: TextStyle(color: Colors.black)),
            onTap: () {
              prefs.logout();
              Navigator.pushNamedAndRemoveUntil(context, 'login',  ModalRoute.withName('login'));
            }
          ),
        ],
      ),
    );
  }
  
}
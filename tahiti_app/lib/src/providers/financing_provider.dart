import 'package:http/http.dart' as http;
import 'package:tahiti_app/src/models/financing_model.dart';
import 'dart:async';
import 'dart:convert';
import 'package:tahiti_app/src/shared_prefs/preferencias_usuario.dart';

class FinancingProvider {

  String _url = 'tutorial-tahiti.000webhostapp.com';
  //String _url = 'f49d83d6adf3.ngrok.io';
  final prefs = PreferenciasUsuario();
  

   Future<List<dynamic>> getFinancings() async {

    final url = Uri.http(_url, '/api/financing');
    final Map<String, String> _headers = {
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Authorization': prefs.token,
    }; 

    final resp = await http.get(
      url,
      headers: _headers,
    );
    final decoded = json.decode(resp.body);

    final financings  = Financings.fromJsonList(decoded['financings']);
  
    return financings.items;
  }

  Future<List<dynamic>> getFinancingsUser(int id) async {

    final url = Uri.http(_url, '/api/financing/user/$id');
    final Map<String, String> _headers = {
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Authorization': prefs.token,
    }; 

    final resp = await http.get(
      url,
      headers: _headers,
    );
    final decoded = json.decode(resp.body);

    final financings  = Financings.fromJsonList(decoded['financings']);
    return financings.items;
  }

  Future trueDeleteFinancing(int id) async {

    final url = Uri.http(_url, '/api/financing/destroy/$id');
    final Map<String, String> _headers = {
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Authorization': prefs.token,
    }; 

    final resp = await http.post(
      url,
      headers: _headers,
    );

    final decoded = json.decode(resp.body);

    return decoded;
  }

  Future postFinancing(Financing financing) async {

    Map dataFinancing = financing.toJson();
    String data = json.encode(dataFinancing);
    print(data);
    String params = 'json='+data;
    

    final url = Uri.http(_url, '/api/financing');
    final Map<String, String> _headers = {
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Authorization': prefs.token,
    }; 

    final resp = await http.post(
      url,
      body: params,
      headers: _headers,
    );

    Map<String, dynamic> decoded = json.decode(resp.body);

    return decoded;
  }

  Future postFinancingUpdate(Financing financing) async {

    Map dataFinancing = financing.toJson();
    String data = json.encode(dataFinancing);
    print(data);
    String params = 'json='+data;
    

    final url = Uri.http(_url, '/api/financing/update');
    final Map<String, String> _headers = {
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Authorization': prefs.token,
    }; 

    final resp = await http.post(
      url,
      body: params,
      headers: _headers,
    );

    Map<String, dynamic> decoded = json.decode(resp.body);

    return decoded;
  }

}
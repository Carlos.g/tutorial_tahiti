import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:tahiti_app/src/models/unit_model.dart';

class ChapterProvider {

  String _url = 'tutorial-tahiti.000webhostapp.com';
  //String _url = '8d603758fd31.ngrok.io';
  

   Future<List<Unit>> getChapters() async {

    final url = Uri.http(_url, '/api/chapter');
    final Map<String, String> _headers = {
      'Content-Type' : 'application/x-www-form-urlencoded',
    }; 

    final resp = await http.get(
      url,
      headers: _headers,
    );
    final decoded = json.decode(resp.body);
    final units  = Units.fromJsonList(decoded['chapters']);
  
    return units.items;
  }

  Future<File> getPdf(String pdfResource) async {
    try{

      final url = Uri.http(_url, '/$pdfResource');
      final data = await http.get(url);
      final bytes = data.bodyBytes;
      final dir = await getApplicationDocumentsDirectory();

      File file = File("${dir.path}/mypdfonline.pdf");

      File urlFile = await file.writeAsBytes(bytes);

      return urlFile; 

    } catch (e){
      throw Exception("Error abriendo el archivo");
    }
  }




}
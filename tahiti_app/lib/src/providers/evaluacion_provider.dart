import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:tahiti_app/src/models/evaluacion_model.dart';


class EvaluacionProvider{

  String _url = 'tutorial-tahiti.000webhostapp.com';

  Future<List<dynamic>> cargarData(String archivo) async {

     final resp = await rootBundle.loadString('data/$archivo');
     final dataMap = json.decode(resp);

     final preguntas = Evaluaciones.fromJsonList(dataMap['preguntas']);

     return preguntas.items;
  }

  Future <Map<String, dynamic>> postGrade(int unidadNum, String token) async {
 
    Map<String, int> dataGrade = {
      'lastUnit' : unidadNum,
    };

    String data = jsonEncode(dataGrade);

    final url = Uri.http(_url, '/api/user/update/grade');
    String params = 'json='+data;

    final Map<String, String> _headers = {
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Authorization': token,
    }; 

    final resp = await http.post(
      url,
      body: params,
      headers: _headers,
    );

    Map<String, dynamic> decoded = json.decode(resp.body);

    return decoded;
  }
}

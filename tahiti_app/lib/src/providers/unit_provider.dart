import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:tahiti_app/src/models/unit_model.dart';
import 'package:tahiti_app/src/shared_prefs/preferencias_usuario.dart';

class UnitProvider {

  final prefs = PreferenciasUsuario();
  String _url = 'tutorial-tahiti.000webhostapp.com';
  //String _url = '8d603758fd31.ngrok.io';

   Future<List<Unit>> getUnits() async {

    final url = Uri.http(_url, '/api/unit');
    final Map<String, String> _headers = {
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Authorization': prefs.token,
    }; 

    final resp = await http.get(
      url,
      headers: _headers,
    );

    final decoded = json.decode(resp.body);
    final units  = Units.fromJsonList(decoded['units']);
  
    return units.items;
  }


}
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:connectivity/connectivity.dart';
//import 'dart:io';

import 'package:tahiti_app/src/models/user_model.dart';
import 'package:tahiti_app/src/shared_prefs/preferencias_usuario.dart';
import 'package:tahiti_app/src/widgets/connection_failed.dart';

class UserProvider { 

 static final UserProvider _instancia = UserProvider._internal();
 final prefs = PreferenciasUsuario();

  factory UserProvider() {
    return _instancia;
  } 

  UserProvider._internal(); 

 String _url = 'tutorial-tahiti.000webhostapp.com';
 //String _url = 'f49d83d6adf3.ngrok.io';
 User user;
 final _connectivity = Connectivity();
  
  Future<dynamic> getIndex() async{
    final url = Uri.http(_url, '/api/user');

    var conectivityResult = await _connectivity.checkConnectivity();

    if(conectivityResult != ConnectivityResult.none){

      final resp = await http.get(url);
      final decoded = json.decode(resp.body);

      final users = Users.fromJsonList(decoded['users']);

      return users.items;
    }

    return ConnectionFailed(); 
  }

  Future <Map<String, dynamic>> postLogin(User userData, {@required bool getIdentity}) async {
 
    user = userData;

    Map dataUser = user.toJson();
    dataUser['getidentity'] = getIdentity;

    String data = jsonEncode(dataUser);
    final url = Uri.http(_url, '/api/login');
    String params = 'json='+data;
    final Map<String, String> _headers = {
      'Content-Type' : 'application/x-www-form-urlencoded',
    }; 

    final resp = await http.post(
      url,
      body: params,
      headers: _headers,
    );

    Map<String, dynamic> decoded = json.decode(resp.body);
    //print(decoded);
    return decoded;

  }

  Future <Map<String, dynamic>> postRegister(User userData, {@required String getRole}) async {
 
    user = userData;

    Map dataUser = user.toJson();
    dataUser['role'] = getRole;

    String data = jsonEncode(dataUser);
    final url = Uri.http(_url, '/api/register');
    String params = 'json='+data;
    final Map<String, String> _headers = {
      'Content-Type' : 'application/x-www-form-urlencoded',
    }; 

    final resp = await http.post(
      url,
      body: params,
      headers: _headers,
    );

    Map<String, dynamic> decoded = json.decode(resp.body);

    return decoded;

  }

  Future <Map<String, dynamic>> postUpload(String imagePath, String token, String fileName) async {

    final dio = Dio();
    dio.options.baseUrl = 'http://$_url';
    dio.options.headers["Authorization"] = token;
    final url = '/api/user/upload';

    FormData formData = FormData.fromMap({
      "file" : await MultipartFile.fromFile(imagePath, filename: fileName),
    });

    final resp = await dio.post(
      url, 
      data: formData
    );

    Map<String, dynamic> decoded = resp.data;

    return decoded ; 
  }

  Future <Map<String, dynamic>> putUser(User user, String token, bool uploadedImage) async {
 
    Map dataUser = user.toJson();
    if(uploadedImage){
      dataUser['deleteResource'] = true;
    }else{
       dataUser['deleteResource'] = false;
    }
    String data = jsonEncode(dataUser);

    final url = Uri.http(_url, '/api/user/update');
    String params = 'json='+data;

    final Map<String, String> _headers = {
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Authorization': token,
    }; 

    final resp = await http.post(
      url,
      body: params,
      headers: _headers,
    );

    Map<String, dynamic> decoded = json.decode(resp.body);

    return decoded;
  }

   revToken() async {

    String token = '';

    if(prefs.token != null){
      token = prefs.token; 
    }

    final url = Uri.http(_url, '/api/init');
    final Map<String, String> _headers = {
      'Authorization': token,
    }; 
    final resp = await http.get(
      url,
      headers: _headers,
    );

    try{

      Map<String, dynamic> decoded = json.decode(resp.body);

      if(decoded['code'] == 400) {
        prefs.logout();
        return;
      }
      return;

    }catch(ex) {
      print(ex);
    }
  }

  
 
}
import 'package:flutter/material.dart';
import 'package:tahiti_app/src/models/user_model.dart';
import 'package:tahiti_app/src/providers/users_provider.dart';
import 'package:tahiti_app/src/shared_prefs/preferencias_usuario.dart';

class RegisterPage extends StatefulWidget {

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  final  prefs = PreferenciasUsuario();
  User _user = User(1,1, '', '', '', '', '', '', '', '');
  UserProvider _userProvider = new UserProvider();
  Future _apiResp;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomRight,
            colors: [
              //Colors.green[50],
              //Colors.green[400],
              Colors.blue[600],
              Colors.red[900],
            ],
          ),
        ),
        child: ListView(
          children: _createRegisterPage(),
        ),
      ),
    );
  }

  List<Widget> _createRegisterPage(){
    return <Widget> [ 
     SizedBox(height: 100.0),
     Center(
       child: Text('Registro', 
         style: TextStyle(
           color: Colors.white,
           fontSize: 40.0,
         ),  
       ),
     ),
     SizedBox(height: 20.0),
     _crearName(),
     SizedBox(height: 20.0),
     _crearSurName(),
     SizedBox(height: 20.0),
     _crearEmail(),
     SizedBox(height: 20.0),
     _crearPassword(),
     (_apiResp == null) ? _botonRegister() : _register(context),
    ];
  }

  Widget _crearEmail(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      width: double.infinity,
      child: TextField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          icon: Icon(Icons.mail, color: Colors.black),
          labelStyle: TextStyle(color: Colors.black),
          labelText: 'Correo',
          focusColor: Colors.black,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
            ),
          ),
        ),
        onChanged: (valor){
          setState(() {
            _user.email = valor;
          });
        },
      ),
    );
  }

  Widget _crearName(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      width: double.infinity,
      child: TextField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          icon: Icon(Icons.text_fields, color: Colors.black),
          labelStyle: TextStyle(color: Colors.black),
          labelText: 'Nombre',
          focusColor: Colors.black,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
            ),
          ),
        ),
        onChanged: (valor){
          setState(() {
            _user.name = valor;
          });
        },
      ),
    );
  }

  Widget _crearSurName(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      width: double.infinity,
      child: TextField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          icon: Icon(Icons.textsms, color: Colors.black),
          labelStyle: TextStyle(color: Colors.black),
          labelText: 'Apellido',
          focusColor: Colors.black,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
            ),
          ),
        ),
        onChanged: (valor){
          setState(() {
            _user.surname = valor;
          });
        },
      ),
    );
  }

  Widget _crearPassword(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      width: double.infinity,
      child: TextField(
        obscureText: true,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          icon: Icon(Icons.lock, color: Colors.black),
          labelStyle: TextStyle(color: Colors.black),
          labelText: 'Contraseña',
          focusColor: Colors.black,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
            ),
          ),
        ),
        onChanged: (valor){
          setState(() {
            _user.password = valor;
          });
        },
      ),
    );
  }

  Widget _botonRegister(){
    return  Container(
      margin: EdgeInsets.symmetric(horizontal: 50.0, vertical: 50.0),
      padding: EdgeInsets.all(0.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(100.0),
        ),
        textColor: Colors.white,
        padding: const EdgeInsets.all(0.0),
        child: Container(
          width: double.infinity,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius:BorderRadius.circular(100.0),
            gradient: LinearGradient(
              colors: <Color>[
                Colors.blue,
                Colors.lightBlue,
                Colors.blue[50],
              ],
            ),
          ),
          padding: EdgeInsets.all(10.0),
          child: Text('Registrate', style: TextStyle(fontSize: 20)),
        ),
        onPressed: () {
          setState(() {
            _apiResp = _userProvider.postRegister(_user, getRole: 'ROLE_USER');
          });
        },
      ),
    );
  }

  Widget _register(context){
    return FutureBuilder (
      future: _apiResp,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        if (!snapshot.hasData){
          return Center(child: CircularProgressIndicator());
        }  
        if(snapshot.data['code'] == 200){
          _apiResp = null;
          return _successAlerta();
        } 
        _apiResp = null;
        return _errorAlerta(snapshot.data);
      }
    );
  }

  Widget _successAlerta() {
    return AlertDialog(
        title: Text('Bienvenido'),
        content: Text('Presiona ok para dirigirte al inicio'),
        backgroundColor: Colors.greenAccent,
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.pushReplacementNamed(context, 'login');
            }, 
            child: Text('Ok'),
          ),
        ],
    );
  }

  Widget _errorAlerta(Map data) {
    return AlertDialog(
        title: Text('Error de registro'),
        content: Text('Datos erroneos'),
        backgroundColor: Colors.red,
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.pushReplacementNamed(context, 'register');
            }, 
            child: Text('Ok'),
          ),
        ],
      );
  }
}
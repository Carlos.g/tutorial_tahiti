import 'package:flutter/material.dart';

import 'package:tahiti_app/src/providers/menu_provider.dart';
import 'package:tahiti_app/src/shared_prefs/preferencias_usuario.dart';
import 'package:tahiti_app/src/widgets/menu_drawer.dart';


class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

  final _prefs = PreferenciasUsuario();
  final _avatarImage = _prefs.getImage();

   return Scaffold(
     drawer: MenuDrawer(),
     appBar: AppBar(
       //leading: Icon(Icons.menu),
       title: Text('Tutorial Tahiti'),
       actions: <Widget>[
         _crearAvatar(_avatarImage, context),
       ],
     ),
     body: _menuList(),
   );
  }


  Widget _menuList() {
    return FutureBuilder(
      initialData: [],
      future: menuProvider.cargarData(),
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(
          children: _listaItem(snapshot.data, context),
        );
      }
    );
  }

  Widget _crearAvatar(String url, BuildContext context) {
    return  GestureDetector(
        child: Container(
        padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
        child: CircleAvatar(
          minRadius: 20.0,
          maxRadius: 20.0,
          backgroundImage: NetworkImage(url),
        ),
      ),
      onTap: () => Navigator.pushNamed(context, 'settings'),
    );
  }

  List<Widget> _listaItem(List<dynamic> data, BuildContext context) {

    List<Widget> items = [];

    items = data.map((item) {
      return Padding(
        padding: EdgeInsets.all(15.0),
        child: GestureDetector(
          child: _tarjeta(item),
          onTap: () => Navigator.pushNamed(context, item['ruta']),
        ),
      );
    }).toList();

    return items;
  }

  Widget _tarjeta(Map<String, dynamic> item){
    return Container(
      width: double.infinity,
      height: 250.0,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage(item['image'])),
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Container(
        alignment: Alignment.bottomCenter,
        child: FractionallySizedBox(
          heightFactor: 0.4,
          widthFactor: 1,
          child: Opacity(
            opacity: 1.0,
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: Colors.black38,
                borderRadius: BorderRadius.vertical(bottom: Radius.circular(20.0)),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(item['titulo'], style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                  Text(item['texto'], style: TextStyle(color: Colors.white)),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

}
import 'package:flutter/material.dart';

import 'package:tahiti_app/src/models/financing_model.dart';
import 'package:tahiti_app/src/providers/financing_provider.dart';
import 'package:tahiti_app/src/shared_prefs/preferencias_usuario.dart';
import 'package:tahiti_app/src/widgets/menu_drawer.dart';
import 'package:validators/validators.dart';

class FinancingCreate extends StatefulWidget {

  final argument;

  FinancingCreate({
    @required this.argument
  });

  @override
  _FinancingCreateState createState() => _FinancingCreateState();
}

class _FinancingCreateState extends State<FinancingCreate> {
  final _financigProvider = FinancingProvider();
  final _prefs = PreferenciasUsuario();
  var _nameController = TextEditingController(text: '');
  var _descriptionController = TextEditingController(text: '');
  var _textoController = TextEditingController(text: '');
  var _financing =  Financing(id: 1, description: '', name: '', link: '', userId: null);
  Map<String, dynamic> _apiRespCreate;
  bool _deshabilitado = true;
  bool _update = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
   @override
  initState() {
    super.initState();
    
    if(widget.argument != 1){
      _financing = widget.argument;
      _update = true;
      _deshabilitado = false;
      _nameController.text = _financing.name;
      _descriptionController.text = _financing.description;
      _textoController.text = _financing.link;
    }

  }

  
  
  @override
  Widget build(BuildContext context) {   

    _financing.userId = _prefs.id;

    return Scaffold(
      key: _scaffoldKey,
      drawer: MenuDrawer(),
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text('Adicionar Financiamiento'),
      ),
      body: SingleChildScrollView(
        child: _crearFormulario(),
      ),
    );
  }

  Widget _crearFormulario() {
    return Container(
      child: Column(children: <Widget>[
        _textoAyuda(),
        _crearNombre(),
        SizedBox(height: 20.0),
        _crearDescripcion(),
        SizedBox(height: 20.0),
        _crearTexto(),
        SizedBox(height: 20.0,),
        _botonCrear(),
      ]),
    );
  }

  Widget _crearNombre() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      width: double.infinity,
      child: TextField(
        controller: _nameController,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          icon: Icon(Icons.text_fields, color: Colors.black),
          labelStyle: TextStyle(color: Colors.black),
          labelText: 'Nombre',
          focusColor: Colors.black,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
            ),
          ),
        ),
        onChanged: (valor) {
          _financing.name = valor;
          _botonDeshabilitado();
          setState(() {
              
            });
        },
      ),
    );
  }

  Widget _crearDescripcion() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      width: double.infinity,
      child: TextField(
        controller: _descriptionController,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          icon: Icon(Icons.text_rotation_angleup, color: Colors.black),
          labelStyle: TextStyle(color: Colors.black),
          labelText: 'Descripcion',
          focusColor: Colors.black,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
            ),
          ),
        ),
        onChanged: (valor) {
           _financing.description = valor;
           _botonDeshabilitado();
           setState(() {
              
            });
        },
      ),
    );
  }

  Widget _crearTexto() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      width: double.infinity,
      child: TextField(
        maxLines: null,
        controller: _textoController,
        keyboardType: TextInputType.multiline,
        decoration: InputDecoration(
          icon: Icon(Icons.aspect_ratio, color: Colors.black),
          labelStyle: TextStyle(color: Colors.black),
          labelText: 'Enlace',
          focusColor: Colors.black,
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
            ),
          ),
        ),
        onChanged: (valor) {
            _financing.link = valor;
            _botonDeshabilitado();
            setState(() {
              
            });
        },
      ),
    );
  }

  Widget _textoAyuda() {
    return Container(
      decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.red[200],
            ),
            BoxShadow(
              color: Colors.white10,
            ),
          ],
          borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(50.0),
              topRight: Radius.circular(50.0),
              bottomLeft: Radius.circular(20.0)
          )
      ),
      margin: EdgeInsets.all(10.0),
      padding: EdgeInsets.all(20.0),
      child: Text(
        '-El campo descripcion es una breve introduccion al metodo de financiamiento \n\n -El campo Enlace es donde debes poner el enlace hacia la pagina web de la entidad que realice el financiamiento',
        style: TextStyle(color: Colors.black, fontSize: 20.0),
      ),
    );
  }

  Widget _botonCrear() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 50.0, vertical: 50.0),
      padding: EdgeInsets.all(0.0),
      child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100.0),
          ),
          textColor: Colors.white,
          padding: const EdgeInsets.all(0.0),
          child: Container(
            width: double.infinity,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100.0),
              gradient: LinearGradient(
                colors: <Color>[
                  Colors.blue,
                  Colors.lightBlue,
                  Colors.blue[50],
                ],
              ),
            ),
            padding: EdgeInsets.all(10.0),
            child: (_update) ? Text('Actualizar', style: TextStyle(fontSize: 20)) : Text('Crear', style: TextStyle(fontSize: 20)) 
          ),
          onPressed: _deshabilitado ? null : _action
      )
    ); 
  }

  void _botonDeshabilitado() {

    if (_nameController.text == '') {_deshabilitado = true; return;}
    
    if (_descriptionController.text == '') {_deshabilitado = true; return;}

    if (_textoController.text == '') {_deshabilitado = true; return;}

    _deshabilitado = false;
  }

  void _crear() async {

    _apiRespCreate = await _financigProvider.postFinancing(_financing);

    if (_apiRespCreate['code'] == 200) {
      _snackShow('Creacion correcta', Colors.green);
      return;
    }

    if (_apiRespCreate['code'] <= 300) {
      _snackShow('Error al crear', Colors.red);
      return;
    }
  }

  void _actualizar() async {

    _apiRespCreate = await _financigProvider.postFinancingUpdate(_financing);

    if (_apiRespCreate['code'] == 200) {
      _snackShow('Actualizacion correcta', Colors.green);
      return;
    }

    if (_apiRespCreate['code'] <= 300) {
      _snackShow('Error al actualizar', Colors.red);
      return;
    }
  }

  dynamic _action() {
    //print('isUrl: ${isURL(_financing.link, requireTld: false)}');

    if(!isURL(_financing.link, requireTld: false)) return _snackShow("el enlace no es valido", Colors.red);

    if(_update) return _actualizar();

    return _crear();
  }

  _snackShow(String msg, Color color) {
    SnackBar snack = SnackBar(
      content: Text(msg),
      backgroundColor: color,
    );
    _scaffoldKey.currentState.showSnackBar(snack);
  }

  @override
  void dispose(){
    
    _nameController.dispose();
    _textoController.dispose();
    _descriptionController.dispose();

    super.dispose();
  }
}

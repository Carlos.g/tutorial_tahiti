import 'package:flutter/material.dart';
import 'package:tahiti_app/src/models/chapter_model.dart';
import 'package:tahiti_app/src/models/unit_model.dart';
import 'package:tahiti_app/src/pages/evaluacion_page.dart';
import 'package:tahiti_app/src/widgets/menu_drawer.dart';
import 'package:url_launcher/url_launcher.Dart';



class ChaptersPage extends StatelessWidget {

    
  @override
  Widget build(BuildContext context) {

    final Unit unidad = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      drawer: MenuDrawer(),
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text('${unidad.name}'),
      ),
      body:ListView(
        padding: EdgeInsets.all(5.0),
        children: _listaCapitulos(unidad, context),
      ),
    );
  }

  List<Widget> _listaCapitulos(Unit unidad, BuildContext context) {

     List<Widget> items = [];

     items = unidad.chapter.map((Chapter capitulo) {
       return GestureDetector(
         child: _tarjeta(capitulo, context),
         onTap: () {},
       );
     }).toList();

     items.add(_evaluacion(_archivoEvaluacion(unidad), context, unidad));
     return items;
  }

  Widget _tarjeta(Chapter capitulo, BuildContext context) {
    return Card(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(2.0),
            child: Container(
              height: 150.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(capitulo.getImg()),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          ExpansionTile(
            leading: Icon(Icons.format_list_numbered),
            title: Text(capitulo.name),
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                height: 70.0,
                child: Text(capitulo.description, style: TextStyle(fontSize: 20.0),),
              ),
              ListTile(
                title: Text('Contenido PDF'),
                leading: Icon(Icons.folder_special),
                trailing: Icon(Icons.keyboard_arrow_right),
                onTap: () {
                  Navigator.pushNamed(context, 'pdf', arguments: capitulo.text);
                },
              ),
              ExpansionTile(
                title: Text('Videos'),
                leading: Icon(Icons.video_library),
                children: _links(context, capitulo.video), 
              ),
              ExpansionTile(
                title: Text('Articulos'),
                leading: Icon(Icons.web),
                children: _links(context, capitulo.paper), 
              ),
              /*ExpansionTile(
                title: Text('Tutoriales'),
                leading: Icon(Icons.videogame_asset),
                children: _tutoriales(context), 
              ),*/
            ],
          ),
        ],
      ),
    );
  }

   Widget _evaluacion(String archivo, BuildContext context, Unit unidad) {
     return GestureDetector(
       child: Card(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(2.0),
              child: Container(
                height: 150.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/img/menu-img.jpg"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Center(child: Text('Quiz', style: TextStyle(fontSize: 30.0, color: Colors.white)))
              ),
            ),
          ],
        ),
       ),
       onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => EvaluacionPage(archivo: archivo, unidadNum: unidad.numeroUnidad(),)))
     );
   }

   List<Widget> _links(BuildContext context, String texto) {
    if(texto == null) return <Widget> [];
    
    int step = 2;
    List<Widget> lista = [];
    List textoSplited = texto.split('\n');
  

    for (var i = 0; i < textoSplited.length; i += step) {

     if(textoSplited[i] != ''){
        step = 2; 
        String url = textoSplited[i + 1];
        if(url != ''){

          Widget selector =  ListTile(
                title: Text(textoSplited[i]),
                trailing: Icon(Icons.arrow_forward),
                onTap: () {
                  launch(url);
                } 
                  
          );

         lista.add(selector);
        }
     }else{
       step = 1;
     }  
    }

    return lista;
  }


  String _archivoEvaluacion( Unit unidad ) {

    List<String> evaluaciones = [
      "evaluacion_l1.json",
      "evaluacion_l2.json",
      "evaluacion_l3.json",
      "evaluacion_l4.json",
      "evaluacion_l5.json",
      "evaluacion_l6.json",
    ];
    
    String archivo = '';
    String unidadNum = unidad.numeroUnidad().toString();
    
    evaluaciones.forEach((eva) { 
      if(eva.contains(unidadNum)) {
        archivo = eva;
      }
    });

    return archivo;
  }

  
}
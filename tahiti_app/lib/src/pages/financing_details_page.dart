import 'package:flutter/material.dart';
import 'package:tahiti_app/src/models/financing_model.dart';
import 'package:tahiti_app/src/widgets/menu_drawer.dart';
import 'dart:math';

class FinancingDetailsPage extends StatelessWidget {
  
  final rnd = Random();

  @override
  Widget build(BuildContext context) {

    final Financing financing = ModalRoute.of(context).settings.arguments; 
    
    return Scaffold(
      drawer: MenuDrawer(),
      body: CustomScrollView(
        slivers: <Widget>[
          _crearAppBar(financing),
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget> [
                SizedBox(height: 5.0,),
                _crearPagina(financing.link),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _crearAppBar(Financing financing) {
    return SliverAppBar( 
     title: Text(financing.name, style: TextStyle(color: Colors.white, fontSize: 20.0), textAlign: TextAlign.center,),
     shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          bottomRight: Radius.circular(50.0),
          bottomLeft:  Radius.circular(50.0),
      )
     ),
     elevation: 2.0,
     expandedHeight: 150.0,
     pinned: true,
     flexibleSpace: FlexibleSpaceBar(
        background: Container(
         height: 20.0,
         decoration: BoxDecoration(
           borderRadius: BorderRadius.only(
             bottomRight: Radius.circular(50.0),
             bottomLeft:  Radius.circular(50.0),
           ),
           gradient: LinearGradient(colors: <Color>[_randomColor(), _randomColor()]),
         ),
         child: Column(
           mainAxisAlignment: MainAxisAlignment.spaceAround,
           children: [
             SizedBox(height: 50.0,),
             Text('Fecha de creacion :${financing.createdAt.toString()}', style: TextStyle(color: Colors.white, fontSize: 15.0),),
             Text('Creado por : Nombre del usuario', style: TextStyle(color: Colors.white, fontSize: 15.0), textAlign: TextAlign.left),
           ],
         ),
       ),
     ),
   );
  }

  Widget _crearPagina(String texto) {
    return Container(
      alignment: Alignment.center,
      child: Text(texto, style: TextStyle(fontSize: 20.0),),
    );
  }


  Color _randomColor(){
    return Color((rnd.nextDouble() * 0xFFFFFF).toInt()).withOpacity(0.4);
  }
}
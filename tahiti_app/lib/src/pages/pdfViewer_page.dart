import 'package:flutter/material.dart';
import 'package:flutter_full_pdf_viewer/flutter_full_pdf_viewer.dart';
import 'package:tahiti_app/src/providers/chapter_provider.dart';

class PdfViewerPage extends StatefulWidget {

  @override
  _PdfViewerPageState createState() => _PdfViewerPageState();
}

class _PdfViewerPageState extends State<PdfViewerPage> {

  @override
  Widget build(BuildContext context) {

    String pdfResource = ModalRoute.of(context).settings.arguments;
    final chapterProvider = ChapterProvider();

    return Center(
      child: FutureBuilder(
        future: chapterProvider.getPdf(pdfResource),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          
          if(!snapshot.hasData) return CircularProgressIndicator(); 

          return PDFViewerScaffold(
           
            path: snapshot.data.path,
          );
        }
      ),
    );
  }
}
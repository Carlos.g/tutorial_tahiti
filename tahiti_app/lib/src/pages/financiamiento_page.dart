import 'package:flutter/material.dart';

import 'package:tahiti_app/src/models/financing_model.dart';
import 'package:tahiti_app/src/providers/financing_provider.dart';
import 'dart:math';
import 'package:tahiti_app/src/widgets/menu_drawer.dart';
import 'package:url_launcher/url_launcher.dart';

class FinanciamientoPage extends StatelessWidget {

  final financingProvider = FinancingProvider();
  final rnd = Random();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MenuDrawer(),
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text('Financiamiento'),
      ),
      body: _crearGridFinancings(context),
    );
  }

  Widget _crearGridFinancings(BuildContext context) {

    final unidades = financingProvider.getFinancings();

    return Padding(
      padding: EdgeInsets.all(2.0),
      child: FutureBuilder(
        future: unidades,
        builder: (context, AsyncSnapshot snapshot) {
          
          if(!snapshot.hasData) return Center(child: CircularProgressIndicator());
          
          return GridView.count(
            padding: EdgeInsets.all(5.0),
            crossAxisCount: 2,
            scrollDirection: Axis.vertical,
            children: _financingCards(snapshot.data, context),
          );
        }
      ),
    );
  }

  List<Widget> _financingCards(List<Financing> data, BuildContext context) {
      List<Widget> items = [];

      items = data.map((financing) {
          return GestureDetector(
            child: _tarjeta(financing),
            onTap: () {
              launch(financing.link);
            },
          );
      }).toList();

     return items; 
  }

  Widget _tarjeta(Financing financing){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 2.0, vertical: 2.0),
      width: double.infinity,
      height: 300.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.bottomRight,
          colors: <Color>[
            _randomColor(),
            _randomColor(),
            _randomColor(),
          ],
        ),
      ),
      
      child: Container(
        alignment: Alignment.center,
        child: FractionallySizedBox(
          heightFactor: 1,
          widthFactor: 1,
          child: Opacity(
            opacity: 1.0,
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(0.2),
              decoration: BoxDecoration(
                color: Colors.black38,
                borderRadius: BorderRadius.vertical(bottom: Radius.circular(20.0), top: Radius.circular(20.0)),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(financing.name, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold), textAlign: TextAlign.center),
                  Text(financing.description, style: TextStyle(color: Colors.white,), overflow: TextOverflow.fade, textAlign: TextAlign.left,),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Color _randomColor(){
    return Color((rnd.nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0);
  }
}
import 'package:flutter/material.dart';
import 'package:tahiti_app/src/models/unit_model.dart';
import 'package:tahiti_app/src/providers/unit_provider.dart';
import 'package:tahiti_app/src/shared_prefs/preferencias_usuario.dart';

import 'package:tahiti_app/src/widgets/menu_drawer.dart';

class TutorialPage extends StatelessWidget {

  final unitProvider = UnitProvider();
  final prefs = PreferenciasUsuario();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: MenuDrawer(),
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text('Tutorial'),
      ),
      body: _crearGridUnidades(context),
    );
  }

  Widget _crearGridUnidades(BuildContext context) {

    final unidades = unitProvider.getUnits();

    return Padding(
      padding: EdgeInsets.all(2.0),
      child: FutureBuilder(
        future: unidades,
        builder: (context, AsyncSnapshot snapshot) {
          
          if(!snapshot.hasData) return Center(child: CircularProgressIndicator());

          return GridView.count(
            padding: EdgeInsets.all(5.0),
            crossAxisCount: 2,
            scrollDirection: Axis.vertical,
            children: _unidadCards(snapshot.data, context),
          );
        }
      ),
    );
  }

  List<Widget> _unidadCards(List<Unit> data, BuildContext context) {
      List<Widget> items = [];
      int proxUnidad = prefs.lastUnit + 1;

      items = data.map((unit) {
          return GestureDetector(
            child: _tarjeta(unit),
            onTap: () {
              
             if(unit.numeroUnidad() <= proxUnidad){
               Navigator.pushNamed(context, 'chapters', arguments: unit); 
             }else{
               _snackShow('No puedes acceder a esta unidad sin que superes las anteriores', Colors.red);
             } 
            }
            
          );
      }).toList();

     return items; 
  }

  Widget _tarjeta(Unit unit){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 2.0),
      width: double.infinity,
      height: 300.0,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: NetworkImage(unit.getImg())),
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Container(
        alignment: Alignment.bottomCenter,
        child: FractionallySizedBox(
          heightFactor: 0.4,
          widthFactor: 1,
          child: Opacity(
            opacity: 1.0,
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(0.2),
              decoration: BoxDecoration(
                color: Colors.black38,
                borderRadius: BorderRadius.vertical(bottom: Radius.circular(20.0)),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(unit.name, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold), textAlign: TextAlign.center),
                  Text(unit.description, style: TextStyle(color: Colors.white,), overflow: TextOverflow.ellipsis),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _snackShow(String msg, Color color) {
    SnackBar snack = SnackBar(content: Text(msg), backgroundColor: color,);
    _scaffoldKey.currentState.showSnackBar(snack);
  } 

}

import 'package:flutter/material.dart';

import 'package:image_picker/image_picker.dart';
import 'package:tahiti_app/src/models/user_model.dart';
import 'package:tahiti_app/src/providers/users_provider.dart';
import 'package:tahiti_app/src/shared_prefs/preferencias_usuario.dart';
import 'package:tahiti_app/src/widgets/menu_drawer.dart';
import 'dart:io';

class UserSettingsPage extends StatefulWidget {

  @override
  _UserSettingsPageState createState() => _UserSettingsPageState();
}

class _UserSettingsPageState extends State<UserSettingsPage> {
  
  static final _prefs = PreferenciasUsuario();
  final _userProvider = UserProvider();
  final _user = User( _prefs.id,1, _prefs.name, _prefs.surname, '', _prefs.email, _prefs.role, _prefs.description, '', _prefs.imageResource);

  final _emailController         = TextEditingController(text: _prefs.email);
  final _nameController          = TextEditingController(text: _prefs.name);
  final _surnameController       = TextEditingController(text: _prefs.surname);
  final _descriptionController   = TextEditingController(text: _prefs.description);

  Map<String, dynamic>_apiRespUpdate;
  Map<String, dynamic>_apiRespUpload;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  File _image;
  String _imageName;
  final picker = ImagePicker();
  bool _uploadedImage = false;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      drawer: MenuDrawer(),
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text('Usuario'),
      ),
      body: ListView(
        children: <Widget>[
          _crearFormulario(),
        ],
      ),
    );
  }

  
  Widget _crearFormulario() {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(height: 20.0,),
          _crearNombre(),
          SizedBox(height: 20.0,),
          _crearSurname(),
          SizedBox(height: 20.0,),
          _crearDescripcion(),
          SizedBox(height: 20.0,),
          _crearEmail(),
          SizedBox(height: 20.0,),
          _crearImagen(),
          SizedBox(),
          _botonActualizar(),
        ],
      ),
    );
  }

  Widget _crearEmail(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      width: double.infinity,
      child: TextField(
        controller: _emailController,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          icon: Icon(Icons.mail, color: Colors.black),
          labelStyle: TextStyle(color: Colors.black),
          labelText: 'Correo',
          focusColor: Colors.black,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
            ),
          ),
        ),
        onChanged: (valor){
          setState(() {
            _user.email = valor;
          });
        },
      ),
    );
  }

  Widget _crearNombre(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      width: double.infinity,
      child: TextField(
        controller: _nameController,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          icon: Icon(Icons.nature_people, color: Colors.black),
          labelStyle: TextStyle(color: Colors.black),
          labelText: 'Nombre',
          focusColor: Colors.black,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
            ),
          ),
        ),
        onChanged: (valor){
          setState(() {
            _user.name = valor;
          });
        },
      ),
    );
  }

  Widget _crearSurname(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      width: double.infinity,
      child: TextField(
        controller: _surnameController,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          icon: Icon(Icons.text_fields, color: Colors.black),
          labelStyle: TextStyle(color: Colors.black),
          labelText: 'Apellido',
          focusColor: Colors.black,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
            ),
          ),
        ),
        onChanged: (valor){
          setState(() {
            _user.surname = valor;
          });
        },
      ),
    );
  }

  Widget _crearDescripcion(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      width: double.infinity,
      child: TextField(
        controller: _descriptionController,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          icon: Icon(Icons.text_rotation_angleup, color: Colors.black),
          labelStyle: TextStyle(color: Colors.black),
          labelText: 'Descripcion',
          focusColor: Colors.black,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
            ),
          ),
        ),
        onChanged: (valor){
          setState(() {
            _user.description = valor;
          });
        },
      ),
    );
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    _imageName = pickedFile.path.split('/').last;
    setState(() {
      try{
        _image = File(pickedFile.path);
      }catch(ex){
        print(ex);
      }
    });
  }

  deleteImage() {
    setState(() {
        try{
          _image = null;
        }catch(ex){
          print(ex);
        }
      });
  }

  Widget _crearImagen(){
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(20.0),
          child: (_image == null) ? Text('no se ha tomado ninguna imagen') : Image.file(_image),
        ),
        (_image == null) ? _botonUpload() : _botonDispose(),
      ] ,
    );
  }

  Widget _botonUpload() {
    return FlatButton.icon(
      icon:  Icon(Icons.image), 
      label: Text('Elegir imagen', style: TextStyle(fontSize: 10.0),),
      onPressed: () async {
        await getImage();
      } 
    );
  }

   _uploadServer() async {   
    _apiRespUpload = await _userProvider.postUpload(_image.path, _prefs.token, _imageName); 

    if(_apiRespUpload['code'] == 200){
      _user.imageResource = _apiRespUpload['image_resource'];
      _user.image = _apiRespUpload['image'];
      _snackShow('Imagen subida al servidor', Colors.green);
      _uploadedImage = true;
      return;
    }else{
      _snackShow('Error al subir imagen', Colors.red);
    }
  } 
  

  Widget _botonDispose() {
    return FlatButton.icon(
      icon:  Icon(Icons.delete), 
      label: Text('Borrar imagen', style: TextStyle(fontSize: 10.0),),
      onPressed: deleteImage,   
    );
  }

  Widget _botonActualizar(){
    return  Container(
      margin: EdgeInsets.symmetric(horizontal: 50.0, vertical: 50.0),
      padding: EdgeInsets.all(0.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(100.0),
        ),
        textColor: Colors.white,
        padding: const EdgeInsets.all(0.0),
        child: Container(
          width: double.infinity,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius:BorderRadius.circular(100.0),
            gradient: LinearGradient(
              colors: <Color>[
                Colors.blue,
                Colors.lightBlue,
                Colors.blue[50],
              ],
            ),
          ),
          padding: EdgeInsets.all(10.0),
          child: Text('Actualizar', style: TextStyle(fontSize: 20)),
        ),
        onPressed: () async {
          if(_image != null) await _uploadServer();
          _actualizar();
        },
      ),
    );
  }

  _actualizar() async{

    _apiRespUpdate = await _userProvider.putUser(_user, _prefs.token, _uploadedImage);


    if(_apiRespUpdate['code'] == 200) {
      _snackShow('Actualizacion correcta', Colors.green);
      _actualizarPrefs(_apiRespUpdate['changes'], _apiRespUpload); 
      return;
    } 

    if(_apiRespUpdate['code'] == 400) {
      _snackShow('Error al actualizar', Colors.red);
      return;
    } 
  }

  _snackShow(String msg, Color color) {
    SnackBar snack = SnackBar(content: Text(msg), backgroundColor: color,);
    _scaffoldKey.currentState.showSnackBar(snack);
  } 

  _actualizarPrefs(Map<String, dynamic> data, Map<String, dynamic> data1) {
    _prefs.description   = data['description'];
    _prefs.name          = data['name'];
    _prefs.surname       = data['surname'];
    _prefs.email         = data['email'];
    if(data1 != null){
     _prefs.imageResource = data1['image_resource'];
     _prefs.image         = data1['image'];
    }
  }

  

  @override
  void dispose(){
    
    _emailController.dispose();
    _nameController.dispose();
    _surnameController.dispose();
    _descriptionController.dispose();

    super.dispose();
  }
   
}

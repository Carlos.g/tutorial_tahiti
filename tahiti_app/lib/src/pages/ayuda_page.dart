import 'package:flutter/material.dart';
import 'package:tahiti_app/src/widgets/menu_drawer.dart';

class AyudaPage extends StatelessWidget {

  final resource = "storage/chapters/pdf/1595973856CAPITULO 1 GENERALIDADES.pdf";


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MenuDrawer(),
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text('Ayuda'),
      ),
      body: Column(
        children: [
          ListTile(
            leading: Icon(Icons.mail),
            title: Text('Uso'),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: ()  => Navigator.pushNamed(context, 'pdf', arguments: resource)
          ),
          ListTile(
            leading: Icon(Icons.help),
            title: Text('Sobre nosotros'),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              return showAboutDialog(context: context, applicationName: 'Tahiti app', applicationVersion: 'v1.0.0', 
               children: <Widget> [ 
                 SelectableText('tahitiapp@gmail.com'),
                 SelectableText('Universidad Distrital Francisco Jose de Calsas, Sede tecnologica'),
               ]
              );
            },
          ),
        ],
      ),
    );
  }
}
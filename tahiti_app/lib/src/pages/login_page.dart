import 'package:flutter/material.dart';
import 'package:tahiti_app/src/models/user_model.dart';
import 'package:tahiti_app/src/providers/users_provider.dart';
import 'package:tahiti_app/src/shared_prefs/preferencias_usuario.dart';

class LoginPage extends StatefulWidget {

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final  prefs = PreferenciasUsuario();
  User _user = User(1, 1, '', '', '', '', '', '', '', '');
  
  UserProvider _userProvider = new UserProvider();
  Future _apiResp;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
             // Colors.green[50],
              //Colors.green[400],
              Colors.blue[600],
              Colors.red[900],
            ],
          ),
        ),
        child: ListView(
          children: _createLoginPage(),
        ),
      ),
    );
   
  }


  List<Widget> _createLoginPage(){
    return <Widget> [ 
      SizedBox(height: 100.0),
      Center(
        child: Text('LogIn', 
          style: TextStyle(
            color: Colors.white,
            fontSize: 40.0,
          ),  
        ),
      ),
      SizedBox(height: 20.0),
      _crearEmail(),
      SizedBox(height: 20.0),
      _crearPassword(),
      SizedBox(height: 20.0),
      _botones(),
      SizedBox(height: 20.0),
      Container(
        width: 10.0,
        child: (_apiResp == null) ? _botonLogin() : _login(context)
      ),
    ];
  }

  Widget _botones() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        FlatButton(
          onPressed: () => Navigator.pushNamed(context, 'register'), 
          child: Text('Registrate'),
        ),
      ],
    );
  }

  Widget _crearEmail(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      width: double.infinity,
      child: TextField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          icon: Icon(Icons.mail, color: Colors.black),
          labelStyle: TextStyle(color: Colors.black),
          labelText: 'Correo',
          focusColor: Colors.black,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
            ),
          ),
        ),
        onChanged: (valor){
          setState(() {
            _user.email = valor;
          });
        },
      ),
    );
  }

  Widget _crearPassword(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      width: double.infinity,
      child: TextField(
        obscureText: true,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          icon: Icon(Icons.lock, color: Colors.black),
          labelStyle: TextStyle(color: Colors.black),
          labelText: 'Contraseña',
          focusColor: Colors.black,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
            ),
          ),
        ),
        onChanged: (valor){
          setState(() {
            _user.password = valor;
          });
        },
      ),
    );
  }

  Widget _botonLogin(){
    return  Container(
      margin: EdgeInsets.symmetric(horizontal: 50.0, vertical: 50.0),
      padding: EdgeInsets.all(0.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(100.0),
        ),
        textColor: Colors.white,
        padding: const EdgeInsets.all(0.0),
        child: Container(
          width: double.infinity,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius:BorderRadius.circular(100.0),
            gradient: LinearGradient(
              colors: <Color>[
                Colors.blue,
                Colors.lightBlue,
                Colors.blue[50],
              ],
            ),
          ),
          padding: EdgeInsets.all(10.0),
          child: Text('Ingresar', style: TextStyle(fontSize: 20)),
        ),
        onPressed: () {
          setState(() {
            _apiResp = _userProvider.postLogin(_user, getIdentity: true);
          });
        },
      ),
    );
  }

  Widget _login(context){
    return FutureBuilder (
      future: _apiResp,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        if (!snapshot.hasData){
          return Center(child: CircularProgressIndicator());
        }  
        if(snapshot.data.containsKey('token')) {
          User user = User.fromJsonLogin(snapshot.data['user']);
          _guardarPrefs(user, snapshot.data['token']);
          return _successAlerta(user);
        } 
        _apiResp = null;
        return _errorAlerta(snapshot.data);
      }
    );
  }

  Widget _successAlerta(User user) {
    return AlertDialog(
        title: Text('Bienvenido ${user.name}'),
        content: Text('Presiona ok para dirigirte al inicio'),
        backgroundColor: Colors.greenAccent,
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.pushReplacementNamed(context, 'home', arguments: user);
            }, 
            child: Text('Ok'),
          ),
        ],
    );
  }

  Widget _errorAlerta(Map data) {
    return AlertDialog(
        title: Text('Error de login'),
        content: Text('Datos erroneos : credenciales incorrectas'),
        backgroundColor: Colors.red,
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.pushReplacementNamed(context, 'login');
            }, 
            child: Text('Ok'),
          ),
        ],
      );
  }

  _guardarPrefs(User user, String token) {
     prefs.id = user.id;
     prefs.lastUnit = user.lastUnit;
     prefs.name = user.name;
     prefs.surname = user.surname;
     prefs.token = token;
     prefs.description = user.description;
     prefs.imageResource = user.imageResource;
     prefs.image = user.image;
     prefs.email = user.email;
     prefs.role = user.role;
  }

}
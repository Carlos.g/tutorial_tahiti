import 'package:flutter/material.dart';
import 'package:tahiti_app/src/models/financing_model.dart';
import 'package:tahiti_app/src/pages/financing_create_page.dart';

import 'package:tahiti_app/src/shared_prefs/preferencias_usuario.dart';
import 'package:tahiti_app/src/widgets/menu_drawer.dart';
import 'package:tahiti_app/src/providers/financing_provider.dart';


class FinancingMenu extends StatefulWidget {
 
  @override
  _FinancingMenuState createState() => _FinancingMenuState();
}

class _FinancingMenuState extends State<FinancingMenu> {

 final _prefs = PreferenciasUsuario();
 final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

 final financingProvider = FinancingProvider(); 
 List<dynamic> financingList;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      drawer: MenuDrawer(),
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text('Menu financiamiento'),
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: _menuList(),
      ),
      floatingActionButton: _botonAgregar(),
    );
  }

  List<Widget> _menuList() {
    return <Widget> [
      Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.red[200],
            ),
            BoxShadow(
              color: Colors.white10,
            ),
          ],
          borderRadius: BorderRadius.only(bottomRight: Radius.circular(50.0), topRight: Radius.circular(50.0), bottomLeft: Radius.circular(20.0) )
        ),
        margin: EdgeInsets.only(bottom: 20.0,),
        padding: EdgeInsets.all(20.0),
        child: Text('En este modulo puedes revisar todas las opciones de metodos de financiamiento creadas por los usuarios o crear las tuyas :3',
          style: TextStyle(color: Colors.black, fontSize: 20.0),
        ),
      ),
      Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              color: Colors.lightGreen,
            ),
            BoxShadow(
              color: Colors.white30,
            ),
          ]
        ),
        margin: EdgeInsets.only(bottom: 20.0),
        child: ListTile(
          leading: Icon(Icons.monetization_on),
          title: Text('Visualizar metodos de financiamiento'),
          shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10.0)),
          trailing: Icon(Icons.arrow_right),
          onTap: () {
            Navigator.pushNamed(context, 'financing');
          },
        ),
      ),
     Container(
       height: 50.0,
       decoration: BoxDecoration(
         gradient: RadialGradient(
           colors: [
             Colors.lightGreen, Colors.green[100], Colors.green[400],   
           ],
           center: AlignmentDirectional.topCenter
         ),
         borderRadius: BorderRadius.only(topRight: Radius.circular(20.0), topLeft: Radius.circular(20.0) )
       ),
       child: Center(child: Text('Financiamientos adicionados por ti')),
     ),
      _financingTable(context), 
      SizedBox(height: 40.0,), 
    ];
  }

 Widget _financingTable(BuildContext context) {
  return FutureBuilder(
     initialData: [],
     future: financingProvider.getFinancingsUser(_prefs.id),
     builder:(BuildContext context, AsyncSnapshot snapshot) {   
       if(!snapshot.hasData) return Center(child: CircularProgressIndicator());
       
       financingList = snapshot.data;
       return SingleChildScrollView(
         scrollDirection: Axis.horizontal, 
         child: Padding(
           padding: const EdgeInsets.all(8.0),
           child: DataTable(
             columns: [
               DataColumn(label: Text('Nombre'), numeric: false),
               DataColumn(label: Text('Descripcion'), numeric: false),
               DataColumn(label: Text('Acciones'), numeric: false),
             ],
             rows: _financingData(financingList),
           ),
         ),
       );
      }
   );   
 }

  List<dynamic> _financingData(List financings) {
    List<dynamic> dataList = 
     financings.map((data) {
      return DataRow(
        cells:[
          DataCell(
            Text('${data.name}'),
          ),
          DataCell(
            Text('${data.description}'),
          ),
          DataCell(
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: _actionIcons(data, context),
            )
          ),
        ] 
      );
    }).toList();

    return dataList;
  }

  List<Widget> _actionIcons(Financing financing, BuildContext context) {
    return <Widget> [
      IconButton(
        icon: Icon(Icons.edit),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => FinancingCreate(argument: financing))).then((value) {
           setState(() {
             
           });
         });
        }
      ),
      IconButton(
        icon: Icon(Icons.delete),
        onPressed: () {
          _deleteFinancing(financing.id);
        }
      ),
    ];
  }

  void _deleteFinancing(int id) async  {
    var resp =  await financingProvider.trueDeleteFinancing(id);
    if(resp['code'] == 200){
      _snackShow('Financiamiento elimiado correctamente', Colors.green);
      Future.delayed(const Duration(milliseconds: 500), () {
        setState(() {});
      });
      return;
    }

    _snackShow('Error al borrar financiamiento', Colors.red);
  }

  Widget _botonAgregar() {
    return FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () {
         Navigator.of(context).push(MaterialPageRoute(builder: (context) => FinancingCreate(argument: 1))).then((value) {
           setState(() {
             
           });
         });
      }
    );
  }

   _snackShow(String msg, Color color) {
    SnackBar snack = SnackBar(content: Text(msg), backgroundColor: color,);
    _scaffoldKey.currentState.showSnackBar(snack);
  } 

}
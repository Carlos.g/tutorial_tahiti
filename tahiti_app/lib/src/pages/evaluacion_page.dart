import 'package:flutter/material.dart';
import 'package:tahiti_app/src/models/evaluacion_model.dart';
import 'package:tahiti_app/src/providers/evaluacion_provider.dart';
import 'package:tahiti_app/src/shared_prefs/preferencias_usuario.dart';

class EvaluacionPage extends StatefulWidget {

  final String archivo;
  final int unidadNum;
  
  EvaluacionPage({@required this.archivo, @required this.unidadNum}); 

  @override
  _EvaluacionPageState createState() => _EvaluacionPageState();
}

class _EvaluacionPageState extends State<EvaluacionPage> {
  
  final _proveedor = EvaluacionProvider();
  final PreferenciasUsuario _prefs = PreferenciasUsuario();
  double puntaje;
  List<Evaluacion> preguntas;
  int index = 0;
  int puntos = 0;
  bool finish = false;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text('Evaluacion Leccion'),
      ),
      body:  _cargandoPreguntas(),
    );
  }

  Widget _cargandoPreguntas() {
    return FutureBuilder(
      initialData: [],
      future: _proveedor.cargarData(widget.archivo),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if(!snapshot.hasData)  return CircularProgressIndicator();

        if(snapshot.data.length == 0) return CircularProgressIndicator();

        if(finish) return _final();

        return SingleChildScrollView(
         child: _crearContenido(snapshot.data)
        );
      }
    );
  }

  Widget _crearContenido(List<Evaluacion> datos) {

    preguntas = datos;

    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        children: [
          //Cabecera
          Row(
            children: [
              Text('Pregunta: $index', style: TextStyle(fontSize: 20.0),),
              Spacer(),
              Text('Puntos: $puntos',  style: TextStyle(fontSize: 20.0)),
            ],
          ),
          SizedBox(
            height: 50.0,
            child: Text('Pregunta', style: TextStyle(fontSize: 30.0)), 
          ),
          //Pregunta
          Container(
            padding: EdgeInsets.all(10.0),
            constraints: BoxConstraints(
              maxHeight: 250.0,
              minHeight: 40.0,
            ),
            decoration: BoxDecoration(
              color: Colors.amber,
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Center(child: Text('${preguntas[index].pregunta}', style: TextStyle(fontSize: 20.0), textAlign: TextAlign.justify,)),
          ),
          _respuestas(preguntas[index]),
        ],
      ),
    );
  }

  Widget _respuestas(Evaluacion pregunta) {

    List<Widget> respuestas = [];
    
    for (var i = 1; i <= pregunta.respuestas[0].length; i++) {

      Widget respUi = RaisedButton(
        elevation: 1.0,
        child: Text('${pregunta.respuestas[0]['$i']}', style: TextStyle(fontSize: 15.0, color: Colors.white), textAlign: TextAlign.justify),
        color: Colors.blue,
        onPressed: () {
          if(pregunta.respuestas[0]['$i'] == pregunta.respuetaC) {
            puntos++;
              if(index <= preguntas.length){
                index++;
                if(index >= preguntas.length) {
                  finish = true;
                }
              }
            setState(() {
            });
          }else{
              if(index < preguntas.length){
                index++;
                if(index >= preguntas.length) {
                  finish = true;
                }
              }
            setState(() {
            });
          }
        }
      );

      respuestas.add(respUi);
    }
    return Column(children: respuestas);
  }


  Widget _final() {
    if(puntos >= preguntas.length*0.7){
     return FutureBuilder(
      future: _proveedor.postGrade((widget.unidadNum), _prefs.token),
      builder: (BuildContext context, AsyncSnapshot  snapshot) {

        if(!snapshot.hasData) return Center(child: CircularProgressIndicator());

        _prefs.lastUnit = snapshot.data['changes']['lastUnit'];

        return  Center(
          child: Container(
            width: double.infinity,
            height: 100.0,
            color: Colors.green,
            child: Center(child: Text('Felicidades has superado el quiz, ya puedes avanzar al proximo', style: TextStyle(color: Colors.white, fontSize: 20.0), textAlign: TextAlign.center,)),
          ),
        );
      }
     );
    }
    return Center(
      child: Container(
          width: double.infinity,
          height: 100.0,
          color: Colors.red,
          child: Center(child: Text('Lo sentimos no has superado el quiz, intentalo de nuevo', style: TextStyle(color: Colors.white, fontSize: 20.0), textAlign: TextAlign.center,)),
      ),
    );

  }

}
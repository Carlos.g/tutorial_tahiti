class Evaluaciones {

  List<Evaluacion> items =[];

  Evaluaciones();

  Evaluaciones.fromJsonList( List<dynamic> jsonList){
    if(jsonList == null) return;

    for(var item in jsonList) {
      final chapter = new Evaluacion.fromJsonMap(item);
      this.items.add(chapter);
    }
  }
}

class Evaluacion {
  int          numero;
  String       pregunta;
  List<dynamic> respuestas;
  String       respuetaC;
  

  Evaluacion({
    this.numero,  
    this.pregunta,
    this.respuestas,
    this.respuetaC
  });

   Evaluacion.fromJsonMap(Map<String, dynamic> json){
    numero  = json['numero'];
    pregunta  = json['pregunta'];
    respuestas  = json['respuestas'];
    respuetaC  = json['respuesta_c'];
  }
}


class Users {
  
  List<User> items = new List();
  
  Users();

  Users.fromJsonList( List<dynamic> jsonList){
      if( jsonList == null)  return;

      for ( var item in jsonList){
        final user = new User.fromJsonMap(item);
        items.add(user);
      }
  }
}

class User {

  int id;
  int lastUnit;
  String name;
  String surname;
  String password;
  String email;
  String role;
  String description;
  String image;
  String imageResource;

  User(
    this.id,                          
    this.lastUnit,                          
    this.name,
    this.surname, 
    this.password,
    this.email, 
    this.role,
    this.description, 
    this.image, 
    this.imageResource, 
  ); 

  Map toJson() => {
    'id' : id,
    'name' : name,
    'lastUnit' : lastUnit,
    'surname' : surname,
    'password' : password,
    'email' : email,
    'role' : role,
    'description' : description,
    'image' : image,
    'imageResource' : imageResource,
  };


  User.fromJsonMap( Map<String, dynamic> json ){
    id            = json['id'];            
    lastUnit      = json['lastUnit'];            
    name          = json['name'];
    surname       = json['surname'];
    password      = json['password'];
    email         = json['email'];
    role          = json['role'];
    description   = json['description'];
    image         = json['image'];
    imageResource = json['image_resource']; 
  }

  User.fromJsonLogin( Map<String, dynamic> json ){
    id            = json['sub'];  
    lastUnit      = json['lastUnit'];          
    name          = json['name'];
    surname       = json['surname'];
    password      = json['password'];
    email         = json['email'];
    role          = json['role'];
    description   = json['description'];
    image         = json['image'];
    imageResource = json['image_resource']; 
  }

  getImage(){
    if(imageResource == null) return 'https://imgclasificados5.emol.com/Publicador/116/F11922917111412972471611595531933226122116.png';

    return 'http://tutorial-tahiti.000webhostapp.com/$imageResource';
  }


}